package task1;

import java.util.Arrays;
import java.util.Comparator;

/**
 * class sorts Integer[] in the order of the sum of the digits of its numbers.
 */
public final class SortingArray {

    /**
     * constructor.
     */
    private SortingArray() { }


    /**
     * counts the sum of digits of a number.
     *
     * @param number - int number
     * @return - int sum of digits of a number
     */
    private static int countSumNumbers(int number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    /**
     * sorts Integer[].
     *
     * @param myArrayInteger - Integer[]
     * @return - sorted Integer[]
     */
    static Integer[] sortArray(Integer[] myArrayInteger) {
        Integer[] tempArray = Arrays.copyOf(myArrayInteger,
                myArrayInteger.length);
        Arrays.sort(tempArray,
                Comparator.comparingInt(SortingArray::countSumNumbers));
        return tempArray;
    }
}
