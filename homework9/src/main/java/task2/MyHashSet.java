package task2;

import java.util.HashSet;

<<<<<<< HEAD
/**
 * class MyHashSet implements mathematical operations on sets:
 * Union, Intersection, Minus, Difference.
 */
class MyHashSet<T> extends HashSet {
    /**
     * exception message.
     */
    private final static String MESSAGE_EXCEPTION = "Arguments cannot be null";

    /**
     * method implements a mathematical operation on sets Union.
     *
     * @param myHashSet1 - first set.
     * @param myHashSet2 - second set.
     * @return - union of sets
     * @throws - IllegalArgumentException if any of the sets is null.
     */
    static <T> MyHashSet<T> getMyUnion(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException(MESSAGE_EXCEPTION);
=======
class MyHashSet<T> extends HashSet {

    static <T> MyHashSet<T> getMyUnion(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException("Arguments cannot be null");
>>>>>>> homework8
        }
        return new MyHashSet<T>() {
            {
                addAll(myHashSet1);
                addAll(myHashSet2);
            }
        };
    }

<<<<<<< HEAD
    /**
     * method implements a mathematical operation on sets Intersection.
     *
     * @param myHashSet1 - first set.
     * @param myHashSet2 - second set.
     * @return - intersection of sets.
     * @throws - IllegalArgumentException if any of the sets is null.
     */
    static <T> MyHashSet<T> getMyIntersection(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException(MESSAGE_EXCEPTION);
        }
        return new MyHashSet<T>() {
=======
    static <T> MyHashSet<T> getMyIntersection(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException("Arguments cannot be null");
        }
        return new MyHashSet<T>(){
>>>>>>> homework8
            {
                addAll(myHashSet1);
                retainAll(myHashSet2);
            }
        };
    }

<<<<<<< HEAD
    /**
     * method implements a mathematical operation on sets Minus.
     *
     * @param myHashSet1 - first set.
     * @param myHashSet2 - second set.
     * @return - minus of sets.
     * @throws - IllegalArgumentException if any of the sets is null.
     */
    static <T> MyHashSet<T> getMyMinus(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException(MESSAGE_EXCEPTION);
=======
    static <T> MyHashSet<T> getMyMinus(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException("Arguments cannot be null");
>>>>>>> homework8
        }
        return new MyHashSet<T>() {
            {
                addAll(myHashSet1);
                removeAll(myHashSet2);
            }
        };
    }

<<<<<<< HEAD
    /**
     * method implements a mathematical operation on sets Difference.
     *
     * @param myHashSet1 - first set.
     * @param myHashSet2 - second set.
     * @return - difference of sets.
     * @throws - IllegalArgumentException if any of the sets is null.
     */
    static <T> MyHashSet<T> getMyDifference(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException(MESSAGE_EXCEPTION);
=======
    static <T>MyHashSet<T> getMyDifference(MyHashSet<T> myHashSet1, MyHashSet<T> myHashSet2) {
        if (myHashSet1 == null || myHashSet2 == null) {
            throw new IllegalArgumentException("Arguments cannot be null");
>>>>>>> homework8
        }
        MyHashSet<T> result = new MyHashSet<>();
        for (Object elementSetOne : myHashSet1) {
            if (!myHashSet2.contains(elementSetOne)) {
                result.add(elementSetOne);
            }
        }
        for (Object elementSetTwo : myHashSet2) {
            if (!myHashSet1.contains(elementSetTwo)) {
                result.add(elementSetTwo);
            }
        }
        return result;
    }
}
