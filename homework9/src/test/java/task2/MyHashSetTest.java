package task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyHashSetTest {
    private MyHashSet<String> set1;
    private MyHashSet<String> set2;
    private MyHashSet<String> exception;
    private MyHashSet<String> actual;

    @Before
    public void initSets() {
        exception = new MyHashSet<>();
        set1 = new MyHashSet<>();
        set1.add("A");
        set1.add("B");
        set2 = new MyHashSet<>();
        set2.add("B");
        set2.add("C");
    }

    @Test
    public void testGetMyUnion() {
        exception.add("A");
        exception.add("B");
        exception.add("C");
        actual = MyHashSet.getMyUnion(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyUnionNullSetOne() {
        actual = MyHashSet.getMyUnion(null, set2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyUnionNullSetTwo() {
        actual = MyHashSet.getMyUnion(set1, null);
    }

    @Test
    public void testGetMyIntersection() {
        exception.add("B");
        actual = MyHashSet.getMyIntersection(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyIntersectionNullSetOne() {
        actual = MyHashSet.getMyIntersection(null, set2);
    }

    public void testGetMyIntersectionNullSetTwo() {
        actual = MyHashSet.getMyIntersection(set1, null);
    }

    @Test
    public void testGetMyMinus() {
        exception.add("A");
        actual = MyHashSet.getMyMinus(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyMinusNullSetOne() {
        actual = MyHashSet.getMyMinus(null, set2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyMinusNullSetTwo() {
        actual = MyHashSet.getMyMinus(set1, null);
    }

    @Test
    public void testGetMyDifference() {
        exception.add("A");
        exception.add("C");
        actual = MyHashSet.getMyDifference(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyDifferenceNullOne() {
        actual = MyHashSet.getMyDifference(null, set2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMyDifferenceNullTwo() {
        actual = MyHashSet.getMyDifference(set1, null);
    }

}
