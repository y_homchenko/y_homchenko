package task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MySetTest {
    private MyHashSet<String> set1;
    private MyHashSet<String> set2;
    private MyHashSet<String> exception;
    private MyHashSet<String> actual;

    @Before
    public void initSets() {
        exception = new MyHashSet<>();
        set1 = new MyHashSet<>();
        set1.add("A");
        set1.add("B");
        set2 = new MyHashSet<>();
        set2.add("B");
        set2.add("C");
    }

    @Test
    public void testGetMyUnion() {
        exception.add("A");
        exception.add("B");
        exception.add("C");
        actual = MyHashSet.getMyUnion(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test
    public void testGetMyIntersection() {
        exception.add("B");
        actual = MyHashSet.getMyIntersection(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test
    public void testGetMyMinus() {
        exception.add("A");
        actual = MyHashSet.getMyMinus(set1, set2);
        Assert.assertEquals(exception, actual);
    }

    @Test
    public void testGetMyDifference() {
        exception.add("A");
        exception.add("C");
        actual = MyHashSet.getMyDifference(set1, set2);
        Assert.assertEquals(exception, actual);
    }
}