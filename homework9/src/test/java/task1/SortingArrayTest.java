package task1;

import org.junit.Assert;
import org.junit.Test;

public class SortingArrayTest {
    private Integer[] myArrayInteger = {159, 1, 958, 111, 100, 10, 1111};

    @Test
    public void testSortArray() {
        Integer[] expected = {1, 100, 10, 111, 1111, 159, 958};
        Integer[] actual = SortingArray.sortArray(myArrayInteger);
        Assert.assertArrayEquals(expected, actual);
    }
}