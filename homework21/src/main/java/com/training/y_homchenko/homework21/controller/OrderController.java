package com.training.y_homchenko.homework21.controller;

import com.training.y_homchenko.homework21.dao.CustomersDao;
import com.training.y_homchenko.homework21.dao.GoodsDao;
import com.training.y_homchenko.homework21.domain.Customers;
import com.training.y_homchenko.homework21.domain.Goods;
import com.training.y_homchenko.homework21.service.OrderService;
import com.training.y_homchenko.homework21.service.PrintListService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Controller product add.
 */
@Controller
public class OrderController {

    private final PrintListService printListService;
    private final OrderService orderService;
    private final CustomersDao customersDao;
    private final GoodsDao goodsDao;

    public OrderController(PrintListService printListService, OrderService orderService, CustomersDao customersDao, GoodsDao goodsDao) {
        this.printListService = printListService;
        this.orderService = orderService;
        this.customersDao = customersDao;
        this.goodsDao = goodsDao;
    }

    @RequestMapping(value = "/orderPage", method = RequestMethod.GET)
    public ModelAndView doGet(HttpSession session, Authentication authentication) {
        Customers customer = customersDao.findByName(authentication.getName());
        String goodsCustomerAlready = printListService.listOrdersGoodsWithGoodsToString(customer);
        final List<Goods> listGoods = goodsDao.getAll();
        session.setAttribute("goodsCustomerAlready", goodsCustomerAlready);
        session.setAttribute("listGoods", listGoods);
        session.setAttribute("customerName", customer.getUsername());
        return new ModelAndView("orderPage");
    }

    @RequestMapping(value = "/orderPage", method = RequestMethod.POST)
    public String doPost(HttpServletRequest request, Authentication authentication) {
        Customers customer = customersDao.findByName(authentication.getName());
        final String item = request.getParameter("goods");
        if (request.getParameter("submit") != null) {
            return "redirect:checkPage";
        } else {
            if (item != null) {
                orderService.addGood(item, customer);
            }
            return "redirect:orderPage";
        }
    }
}
