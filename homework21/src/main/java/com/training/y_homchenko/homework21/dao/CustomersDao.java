package com.training.y_homchenko.homework21.dao;

import com.training.y_homchenko.homework21.domain.Customers;

public interface CustomersDao {

    Customers findByName(String name);
}
