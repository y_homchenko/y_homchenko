package com.training.y_homchenko.homework21.service;

import com.training.y_homchenko.homework21.dao.CustomersDao;
import com.training.y_homchenko.homework21.dao.GoodsDao;
import com.training.y_homchenko.homework21.dao.OrdersGoodsDao;
import com.training.y_homchenko.homework21.domain.Customers;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * List of goods to String.
 */
@Service
public class PrintListService {

    private final GoodsDao goodsDao;
    private final CustomersDao customersDao;
    private final OrdersGoodsDao ordersGoodsDao;

    public PrintListService(GoodsDao goodsDao, CustomersDao customersDao, OrdersGoodsDao ordersGoodsDao) {
        this.goodsDao = goodsDao;
        this.customersDao = customersDao;
        this.ordersGoodsDao = ordersGoodsDao;
    }

    /**
     * List OrdersGoods with Goods to String.
     *
     * @param customer - customer
     * @return - String selected items Goods with total_price
     */
    public String listOrdersGoodsWithGoodsToString(Customers customer) {
        Customers customers = customersDao.findByName(customer.getUsername());
        StringBuilder result = new StringBuilder();
        BigDecimal total = new BigDecimal(0);
        final List<Long> orderGoods = ordersGoodsDao.findByOrderId(customers.getId());
        for (int i = 0; i < orderGoods.size(); i++) {
            result.append("<p>").append(i + 1).append(") ").append(goodsDao.findById(orderGoods.get(i)).getTitle())
                    .append(" ").append(goodsDao.findById(orderGoods.get(i)).getPrice()).append("$</p>");
            total = total.add(goodsDao.findById(orderGoods.get(i)).getPrice());
        }
        result.append("<h4>Total: $ ").append(total).append("</h4>");
        return result.toString();
    }
}
