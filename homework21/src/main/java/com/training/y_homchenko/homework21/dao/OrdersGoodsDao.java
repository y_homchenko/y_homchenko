package com.training.y_homchenko.homework21.dao;

import com.training.y_homchenko.homework21.domain.OrdersGoods;

import java.util.List;

public interface OrdersGoodsDao {

    void add(OrdersGoods orderGood);

    List<Long> findByOrderId(Long orderId);

}
