package com.training.y_homchenko.homework21.domain;

import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class OrdersGoods {

    private Long id;
    private Long orderId;
    private Long goodId;

    public OrdersGoods() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdersGoods orderGood = (OrdersGoods) o;
        return id == orderGood.id &&
                orderId == orderGood.orderId &&
                goodId == orderGood.goodId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderId, goodId);
    }

    @Override
    public String toString() {
        return "OrderGood{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", goodId=" + goodId +
                '}';
    }
}
