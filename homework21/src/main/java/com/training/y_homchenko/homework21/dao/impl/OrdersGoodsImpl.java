package com.training.y_homchenko.homework21.dao.impl;

import com.training.y_homchenko.homework21.dao.OrdersGoodsDao;
import com.training.y_homchenko.homework21.domain.OrdersGoods;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OrdersGoods table implementation OrdersGoodsDao.
 */
@Repository
public class OrdersGoodsImpl implements OrdersGoodsDao {

    private final String SQL_ADD = "INSERT INTO ORDERS_GOODS (ORDER_ID, GOOD_ID) VALUES(?, ?)";
    private final String SQL_FIND_BY_ORDER_ID = "SELECT GOOD_ID FROM ORDERS_GOODS WHERE ORDER_ID=?";

    private final JdbcTemplate jdbcTemplate;

    public OrdersGoodsImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Insert instance OrdersGoods to table ORDERS_GOODS.
     *
     * @param orderGood - instance OrdersGoods
     */
    @Override
    public void add(OrdersGoods orderGood) {
        jdbcTemplate.update(SQL_ADD, orderGood.getOrderId(), orderGood.getGoodId());
    }

    /**
     * Find elements by order_id.
     *
     * @param orderId - Long order_id
     * @return - List elements of table ORDERS_GOODS
     */
    @Override
    public List<Long> findByOrderId(Long orderId) {
        return jdbcTemplate.queryForList(SQL_FIND_BY_ORDER_ID, new Object[]{orderId}, Long.class);
    }
}
