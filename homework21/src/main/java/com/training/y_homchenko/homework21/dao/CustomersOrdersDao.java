package com.training.y_homchenko.homework21.dao;

import com.training.y_homchenko.homework21.domain.CustomerOrder;

public interface CustomersOrdersDao {

    CustomerOrder findByUserId(Long userId);

    void update(CustomerOrder customerOrder);
}
