package com.training.y_homchenko.homework21.service;

import com.training.y_homchenko.homework21.dao.CustomersDao;
import com.training.y_homchenko.homework21.dao.CustomersOrdersDao;
import com.training.y_homchenko.homework21.dao.GoodsDao;
import com.training.y_homchenko.homework21.dao.OrdersGoodsDao;
import com.training.y_homchenko.homework21.domain.CustomerOrder;
import com.training.y_homchenko.homework21.domain.Customers;
import com.training.y_homchenko.homework21.domain.OrdersGoods;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Service add good to OrderGoods.
 */
@Service
public class OrderService {

    /**
     * Number of characters after point
     */
    private final int scale = 2;
    private final String MESSAGE_NUMBER_FORMAT_EXCEPTION = "Incorrect argument accountBalance";
    private final String REGEX_SPLIT = " ";
    private final String TARGET_REPLACE = "$";
    private final String REPLACEMENT_REPLACE = "";

    private final CustomersDao customersDao;
    private final OrdersGoodsDao ordersGoodsDao;
    private final GoodsDao goodsDao;
    private final OrdersGoods ordersGoods;
    private final CustomersOrdersDao customersOrdersDao;

    public OrderService(CustomersDao customersDao, OrdersGoodsDao ordersGoodsDao, GoodsDao goodsDao, OrdersGoods ordersGoods, CustomersOrdersDao customersOrdersDao) {
        this.customersDao = customersDao;
        this.ordersGoodsDao = ordersGoodsDao;
        this.goodsDao = goodsDao;
        this.ordersGoods = ordersGoods;
        this.customersOrdersDao = customersOrdersDao;
    }

    /**
     * Add good to OrderGoods.
     *
     * @param item     - good
     * @param customer - customer
     */
    public void addGood(String item, Customers customer) {
        Customers customers = customersDao.findByName(customer.getUsername());
        String[] str = item.split(REGEX_SPLIT);
        String title = str[0];
        BigDecimal price;
        Long goodId;
        price = parseBigDecimal(str[1].replace(TARGET_REPLACE, REPLACEMENT_REPLACE), scale);
        Long userId = customers.getId();
        final CustomerOrder customerOrder = customersOrdersDao.findByUserId(userId);
        Long orderId;
        orderId = customerOrder.getId();
        BigDecimal totalPrice = customerOrder.getTotalPrice();
        totalPrice = totalPrice.add(price);
        customerOrder.setTotalPrice(totalPrice);
        customersOrdersDao.update(customerOrder);
        goodId = goodsDao.findByTitle(title).getId();
        ordersGoods.setGoodId(goodId);
        ordersGoods.setOrderId(orderId);
        ordersGoodsDao.add(ordersGoods);
    }

    /**
     * Method for parsing String to BigDecimal argument with an accuracy of "scale" characters after the point.
     *
     * @param accountBalance - actual argument
     * @param scale          - number of characters after point
     * @return - argument BigDecimal or NumberFormatException
     */
    BigDecimal parseBigDecimal(String accountBalance, int scale) {
        try {
            return new BigDecimal(accountBalance).setScale(scale, BigDecimal.ROUND_HALF_UP);
        } catch (NumberFormatException e) {
            throw new NumberFormatException(MESSAGE_NUMBER_FORMAT_EXCEPTION);
        }
    }
}
