package com.training.y_homchenko.homework21.dao;

import com.training.y_homchenko.homework21.domain.Goods;

import java.util.List;

public interface GoodsDao {

    List<Goods> getAll();

    Goods findByTitle(String title);

    Goods findById(Long id);

}
