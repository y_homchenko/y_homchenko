package com.training.y_homchenko.homework21.service;

import com.training.y_homchenko.homework21.config.WebConfig;
import com.training.y_homchenko.homework21.domain.Customers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = WebConfig.class)
public class PrintListServiceTest {
    @Autowired
    private PrintListService printListService;

    @Test
    public void testListOrdersGoodsWithGoodsToString() {
        //given:
        Customers customer = new Customers();
        customer.setId(1L);
        customer.setUsername("User1");
        customer.setPassword("$2a$04$i/eDtmGlaU222OKvA8/YHuhW8rpf/XmCEGWmhU/PnSjTEJsEXTany");
        customer.setRole("ROLE_ADMIN");
        String expected = "<p>1) table 50.00$</p><h4>Total: $ 50.00</h4>";
        //when:
        String actual = printListService.listOrdersGoodsWithGoodsToString(customer);
        //then:
        assertEquals(expected, actual);
    }
}