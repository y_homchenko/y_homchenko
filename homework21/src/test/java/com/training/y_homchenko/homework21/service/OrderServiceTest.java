package com.training.y_homchenko.homework21.service;

import com.training.y_homchenko.homework21.config.WebConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = WebConfig.class)

public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void testParseBigDecimal() {
        //given:
        BigDecimal expected = new BigDecimal(5.00).setScale(2, BigDecimal.ROUND_HALF_UP);
        //when:
        BigDecimal actual = orderService.parseBigDecimal("5.00", 2);
        //then:
        assertEquals(expected, actual);
    }

    @Test(expected = NumberFormatException.class)
    public void testParseBigDecimalNotNumber() {
        //when:
        orderService.parseBigDecimal("number", 2);
    }

    @Test
    public void testParseBigDecimalNotTwoNumbersAfterPoints() {
        //given:
        BigDecimal expected = new BigDecimal(5.0001).setScale(2, BigDecimal.ROUND_HALF_UP);
        //when:
        BigDecimal actual = orderService.parseBigDecimal("5.00", 2);
        //then:
        assertEquals(expected, actual);
    }
}
