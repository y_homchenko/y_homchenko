IntelliJ IDEA:
Maven -> Plugins -> tomcat7 -> tomcat7:run -> in a browser: localhost:8080

Command Line:
mvn clean install tomcat7:run -> in a browser: localhost:8080

Users:
nameUser: User1   password: Qwerty123   role: ADMIN
nameUser: User2   password: 12345678    role: USER 
nameUser: User3   password: 12345678    role: ANONYMOUS 