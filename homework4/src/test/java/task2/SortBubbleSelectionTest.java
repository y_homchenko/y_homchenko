package task2;

import org.junit.Test;

public class SortBubbleSelectionTest {

    @Test
    public void testMainBubble() {
        SortBubbleSelection.main(new String[]{"1"});
    }

    @Test
    public void testMainSelection() {
        SortBubbleSelection.main(new String[]{"2"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainBubbleNoArg() {
        SortBubbleSelection.main(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainBubbleArg() {
        SortBubbleSelection.main(new String[]{"3"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainBubbleArgChar() {
        SortBubbleSelection.main(new String[]{"c"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainSelectionNoArg() {
        SortBubbleSelection.main(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainSelectionArg() {
        SortBubbleSelection.main(new String[]{"3"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainSelectionArgChar() {
        SortBubbleSelection.main(new String[]{"c"});
    }

}