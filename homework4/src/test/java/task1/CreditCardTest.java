package task1;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class CreditCardTest {

    // constructor Card(String cardHolderName)
    CreditCard cardName = new CreditCard("Yuriy Homchenko");

    @Test
    public void testGetCardHolderNameCardName() {
        String expected = "Yuriy Homchenko";
        String actual = cardName.getCardHolderName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetFromAccountBalanceCardName() {
        BigDecimal expected = new BigDecimal(0);
        BigDecimal actual = cardName.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddToAccountBalanceCardName() {
        BigDecimal expected = new BigDecimal(1000).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardName.addToAccountBalance("1000");
        BigDecimal actual = cardName.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddToAccountBalanceNegativeCardName() {
        cardName.addToAccountBalance("-1000");
    }

    @Test
    public void testWithdrawAccountBalanceCardName() {
        BigDecimal expected = new BigDecimal(500).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardName.addToAccountBalance("1000");
        cardName.withdrawAccountBalance("500");
        BigDecimal actual = cardName.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawAccountBalanceNegativeCardName() {
        BigDecimal expected = new BigDecimal(-100).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardName.withdrawAccountBalance("100");
        BigDecimal actual = cardName.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    // constructor Card(String holderName, String accountBalance)
    CreditCard cardNameAccountBalance = new CreditCard("Yuriy Homchenko", "1000");

    @Test
    public void testGetCardHolderName() {
        String expected = "Yuriy Homchenko";
        String actual = cardNameAccountBalance.getCardHolderName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetFromAccountBalance() {
        BigDecimal expected = new BigDecimal(1000).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal actual = cardNameAccountBalance.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddToAccountBalance() {
        BigDecimal expected = new BigDecimal(2000).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardNameAccountBalance.addToAccountBalance("1000");
        BigDecimal actual = cardNameAccountBalance.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddToAccountBalanceNegative() {
        cardNameAccountBalance.addToAccountBalance("-2000");
    }

    @Test
    public void testWithdrawAccountBalance() {
        BigDecimal expected = new BigDecimal(500).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardNameAccountBalance.withdrawAccountBalance("500");
        BigDecimal actual = cardNameAccountBalance.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawAccountBalanceNegative() {
        BigDecimal expected = new BigDecimal(-1000).setScale(2, BigDecimal.ROUND_HALF_UP);
        cardNameAccountBalance.withdrawAccountBalance("2000");
        BigDecimal actual = cardNameAccountBalance.getFromAccountBalance();
        Assert.assertEquals(expected, actual);
    }
}