package task2;

class SortingContext {

    private Sorter sorter;

    SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    void sort(int[] array) {
        sorter.sort(array);
    }
}
