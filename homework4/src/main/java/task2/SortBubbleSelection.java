package task2;

/**
 * class SortBubbleSelection sorts array int[] using BubbleSort and SelectionSort (Strategy pattern)
 */
public class SortBubbleSelection {

    /**
     * @param args Command line argument int 1 - BubbleSort or 2 - SelectionSort
     */
    public static void main(String[] args) {
        int[] array = {2, 5, 1, 6, 10, 9};
        Sorter sortStrategy;
        int sortingAlgorithmNumber;
        if (args.length == 1) {
            sortingAlgorithmNumber = parseArgInt(args[0]);
        } else {
            throw new IllegalArgumentException("Invalid number of arguments.");
        }
        if (sortingAlgorithmNumber == 1) {
            sortStrategy = new BubbleSort();
        } else if (sortingAlgorithmNumber == 2) {
            sortStrategy = new SelectionSort();
        } else {
            throw new IllegalArgumentException("Incorrect argument (1 - BubbleSort or 2 - SelectionSort");
        }
        SortingContext context = new SortingContext(sortStrategy);
        context.sort(array);

        for (int elem : array) {
            System.out.print(elem + " ");
        }
    }

    /**
     * Method for parse argument Integer
     *
     * @param arg actual argument
     * @return parsed argument
     */
    private static int parseArgInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }
}
