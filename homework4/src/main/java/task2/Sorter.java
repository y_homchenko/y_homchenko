package task2;

/**
 * Interface for Strategy
 */
public interface Sorter {
    void sort(int[] array);
}
