package task1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * ATM class reads user input from the console and performs the selected actions with the card passed to it
 */
public class Atm {

    private boolean sessionOn = true;

    /**
     * displays the ATM menu in the console
     */
    private void mainMenu() {
        System.out.println("For continue work with system input:");
        System.out.print("0 - For exit\n" +
                "1 - for get from account balance\n" +
                "2 - for add account balance\n" +
                "3 - cash withdrawal.\n" +
                "4 - card balance in USD.\n");
    }

    /**
     * the method reads the user's selection from the console and performs the corresponding action from card
     *
     * @param cardInAtm - ATM card
     */
    public void sessionAtmMain(Card cardInAtm) {

        while (sessionOn) {
            mainMenu();
            Scanner in = new Scanner(System.in);
            int numMenu = 0;

            try {
                numMenu = in.nextInt();
                in.nextLine();
            } catch (InputMismatchException e) {
                System.out.println(e);
                System.out.println("You must choose 0, 1, 2, 3, 4!");
                continue;
            }

            switch (numMenu) {
                case 0:
                    System.out.print("You came out of the program!");
                    sessionOn = false;
                    break;
                case 1:
                    System.out.println("Your balance " + cardInAtm.getFromAccountBalance() + "\n");
                    break;
                case 2:
                    System.out.print("Enter amount ");
                    String sumToReplenish = in.nextLine();
                    cardInAtm.addToAccountBalance(sumToReplenish);
                    System.out.println("You replenished your account on " + sumToReplenish);
                    System.out.println("Now your balance " + cardInAtm.getFromAccountBalance() + "\n");
                    break;
                case 3:
                    System.out.print("Enter amount ");
                    String sumToWithdraw = in.nextLine();
                    cardInAtm.withdrawAccountBalance(sumToWithdraw);
                    System.out.println("You have withdrawn the amount " + sumToWithdraw);
                    System.out.println("Now your balance " + cardInAtm.getFromAccountBalance() + "\n");
                    break;
                case 4:
                    cardInAtm.displayCardBalanceToUsd("2.0539");
                    break;
                default:
                    System.out.print("Error!\nSession close.\n");
                    System.out.println("Incorrect argument accountBalance");
            }
        }
    }
}
