package task1;

/**
 * class DebitCard to store cardholder name and account balance
 * and to get/ to add /to withdraw from account balance
 * and to display Card balance in USD
 * the account balance is not negative
 */
public class DebitCard extends Card {

    public DebitCard(String cardHolderName) {
        super(cardHolderName);
    }

    public DebitCard(String holderName, String accountBalance) {
        super(holderName, accountBalance);
    }
}