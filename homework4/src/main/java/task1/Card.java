package task1;

import java.math.BigDecimal;

/**
 * class Card to store cardholder name and account balance
 * and to get/ to add /to withdraw from account balance
 * and to display Card balance in USD
 */
public class Card {

    private String cardHolderName;
    private BigDecimal accountBalance;

    /**
     * number of characters after point
     */
    private int scale = 2;

    /**
     * single-field constructor cardHolderName
     * the field accountBalance takes a default value of 0
     *
     * @param cardHolderName - cardholder name
     */
    public Card(String cardHolderName) {
        this.cardHolderName = cardHolderName;
        this.accountBalance = new BigDecimal(0);
    }

    /**
     * constructor with two fields
     *
     * @param holderName     - cardholder name
     * @param accountBalance - initial balance value
     */
    public Card(String holderName, String accountBalance) {
        cardHolderName = holderName;
        if (parseBigDecimal(accountBalance, scale).compareTo(BigDecimal.ZERO) >= 0) {
            this.accountBalance = parseBigDecimal(accountBalance, scale);
        } else {
            throw new IllegalArgumentException("Incorrect argument accountBalance");
        }
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * returns the name of the cardholder
     *
     * @return - cardholder name
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * returns card balance
     *
     * @return - card balance
     */
    public BigDecimal getFromAccountBalance() {
        return accountBalance;
    }

    /**
     * adds the amount to the card balance
     *
     * @param amountAdd - amount of addition is positive, else an exception IllegalArgumentException
     */
    public void addToAccountBalance(String amountAdd) {
        if ((parseBigDecimal(amountAdd, scale)).compareTo(BigDecimal.ZERO) > 0) {
            accountBalance = accountBalance.add(parseBigDecimal(amountAdd, scale));
        } else {
            throw new IllegalArgumentException("Incorrect argument amountAdd");
        }
    }

    /**
     * withdrawal if the residue after deduction is not negative, else an exception IllegalArgumentException
     *
     * @param amountWithdraw - withdrawal amount
     */
    public void withdrawAccountBalance(String amountWithdraw) {
        if (accountBalance.subtract(parseBigDecimal(amountWithdraw, scale)).compareTo(BigDecimal.ZERO) >= 0) {
            accountBalance = accountBalance.subtract(parseBigDecimal(amountWithdraw, scale));
        } else {
            throw new IllegalArgumentException("Incorrect argument amountWithdraw");
        }
    }

    /**
     * display of balance in currency to exchangeCurrency
     *
     * @param exchangeRate - exchange rate with an accuracy of 4 digits after the point
     */
    public void displayCardBalanceToUsd(String exchangeRate) {
        String accountCurrency = "BYN";
        String exchangeCurrency = "USD";
        BigDecimal accountBalanceByn = getFromAccountBalance();
        BigDecimal rate = parseBigDecimal(exchangeRate, 4);
        System.out.println("Account balance " + accountCurrency + " = " + accountBalanceByn);
        System.out.println("Account balance " + exchangeCurrency + " = " + accountBalanceByn.divide(rate, scale, BigDecimal.ROUND_HALF_UP));
    }

    /**
     * Method for parsing String to BigDecimal argument with an accuracy of "scale" characters after the point
     *
     * @param accountBalance - actual argument
     * @param scale          - number of characters after point
     * @return - argument BigDecimal or NumberFormatException
     */
    protected BigDecimal parseBigDecimal(String accountBalance, int scale) {
        try {
            return new BigDecimal(accountBalance).setScale(scale, BigDecimal.ROUND_HALF_UP);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Incorrect argument accountBalance");
        }
    }
}
