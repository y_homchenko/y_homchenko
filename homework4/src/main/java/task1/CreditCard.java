package task1;

/**
 * class CreditCard to store cardholder name and account balance
 * and to get/ to add /to withdraw from account balance
 * and to display Card balance in USD
 * the account balance may be negative
 */
public class CreditCard extends Card {

    public CreditCard(String cardHolderName) {
        super(cardHolderName);
    }

    public CreditCard(String holderName, String accountBalance) {
        super(holderName, accountBalance);
    }

    /**
     * cash withdrawal
     *
     * @param amountWithdraw - withdrawal amount
     */
    @Override
    public void withdrawAccountBalance(String amountWithdraw) {
        setAccountBalance(getAccountBalance().subtract(parseBigDecimal(amountWithdraw, getScale())));
    }
}
