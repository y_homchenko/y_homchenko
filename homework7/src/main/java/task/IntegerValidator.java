package task;

public class IntegerValidator implements Validator<Integer> {

    /**
     * The method checks the integer according to the given conditions.
     *
     * @param integer given a Integer.
     * @return the result of checking the condition,
     * or if object is null, or does not satisfy the condition then an exception ValidationFailedException
     */

    @Override
    public boolean validate(Integer integer) {
        if (integer == null || integer < 1 || integer > 10) {
            throw new ValidationFailedException("It does not satisfy the condition");
        }
        return true;
    }
}
