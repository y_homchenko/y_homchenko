package task;

import java.util.HashMap;
import java.util.Map;

class ValidationSystem {
    private static final Map<String, Validator> VALIDATOR_TYPE = new HashMap<String, Validator>() {
        {
            put("Integer", new IntegerValidator());
            put("String", new StringValidator());
        }
    };

    /**
     * The method validates objects according to the given conditions.
     *
     * @param object any reference type.
     * @return the result of the validation of the transferred object,
     * or if object is null then an exception ValidationFailedException
     */
    static boolean validate(Object object) {
        if (object == null) {
            throw new ValidationFailedException("It's can't be null!");
        }
        return returnTypeValidator(object.getClass().getSimpleName()).validate(object);
    }

    private static Validator returnTypeValidator(String key) {
        if (VALIDATOR_TYPE.containsKey(key)) {
            return VALIDATOR_TYPE.get(key);
        }
        return null;
    }
}
