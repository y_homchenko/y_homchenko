package task;

public interface Validator<T> {
    boolean validate(T validatedType) throws ValidationFailedException;
}
