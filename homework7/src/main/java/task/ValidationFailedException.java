package task;

class ValidationFailedException extends RuntimeException {
    ValidationFailedException(String message) {
        super(message);
    }
}
