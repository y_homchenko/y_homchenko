package task;

import java.util.regex.Pattern;

public class StringValidator implements Validator<String> {
    /**
     * uppercase at the beginning of the line
     */
    private static final String NOT_FIRST_CHAR = "^[^A-ZА-ЯЁ].*$";

    /**
     * The method checks the String according to the given conditions.
     *
     * @param string given a string.
     * @return the result of checking the condition,
     * or if object is null, or does not satisfy the condition then an exception ValidationFailedException
     */
    @Override
    public boolean validate(String string) {
        if (string == null || string.isEmpty() || Pattern.matches(NOT_FIRST_CHAR, string)) {
            throw new ValidationFailedException("It does not satisfy the condition");
        }
        return true;
    }
}
