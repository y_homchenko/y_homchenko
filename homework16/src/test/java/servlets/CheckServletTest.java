package servlets;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CheckServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private PrintWriter writer;
    @Mock
    private Cart cart;

    @Test
    public void testDoGet() throws IOException, ServletException {

        //given:
        final ArrayList<ConcurrentHashMap<String, Double>> order = new ArrayList<>();
        order.add(new ConcurrentHashMap<String, Double>() {{
            put("test", 5d);
        }});

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(cart.getOrder()).thenReturn(order);
        when(response.getWriter()).thenReturn(writer);

        //when:
        new CheckServlet().doGet(request, response);

        //then:
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoGetCartIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(null);

        //when:
        new CheckServlet().doGet(request, response);

        //then:
        verify(response).sendRedirect("/start");
    }

    @Test
    public void testDoGetOrderIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(cart.getOrder()).thenReturn(null);

        //when:
        new CheckServlet().doGet(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }
}
