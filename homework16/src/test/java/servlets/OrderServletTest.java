package servlets;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private PrintWriter writer;
    @Mock
    private Cart cart;

    @Test
    public void testDoGet() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(response.getWriter()).thenReturn(writer);

        //when:
        new OrderServlet().doGet(request, response);

        //then:
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoPostRedirectCheck() throws ServletException, IOException {

        //given:
        String item = "test 5$";
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("goods")).thenReturn(item);
        when(request.getParameter("submit")).thenReturn("submit");

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/check");
    }

    @Test
    public void testDoPost() throws ServletException, IOException {

        //given:
        String item = "test 5$";
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("goods")).thenReturn(item);
        when(request.getParameter("addItem")).thenReturn("addItem");

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }

    @Test
    public void testDoPostItemsIsNull() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("addItem")).thenReturn("addItem");
        when(request.getParameterValues("goods")).thenReturn(null);

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }
}
