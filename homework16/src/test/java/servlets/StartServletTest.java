package servlets;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class StartServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private PrintWriter writer;
    @Mock
    private Cart cart;

    @Test
    public void testDoGet() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(response.getWriter()).thenReturn(writer);

        //when:
        new StartServlet().doGet(request, response);

        //then:
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoPost() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn("Test");
        when(request.getParameter("checkBoxAgree")).thenReturn("true");

        //when:
        new StartServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }

    @Test
    public void testDoPostCartIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn(null);
        when(request.getParameter("checkBoxAgree")).thenReturn("true");

        //when:
        new StartServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/start");
    }

    @Test
    public void testDoPostCheckBoxAgreeIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn("Test");
        when(request.getParameter("checkBoxAgree")).thenReturn(null);

        //when:
        new StartServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }

    @Test
    public void testDoPostNameCustomerAndCheckBoxAgreeIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn(null);
        when(request.getParameter("checkBoxAgree")).thenReturn(null);

        //when:
        new StartServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/start");
    }
}
