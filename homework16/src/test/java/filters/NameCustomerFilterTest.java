package filters;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NameCustomerFilterTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private FilterChain filterChain;
    @Mock
    private Cart cart;

    @Test
    public void testDoFilter() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);

        //when:
        new NameCustomerFilter().doFilter(request, response, filterChain);

        //then:
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void testDoFilterNameCustomerIsNull() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(null);

        //when:
        new NameCustomerFilter().doFilter(request, response, filterChain);

        //then:
        verify(response).sendRedirect("/start");
    }
}
