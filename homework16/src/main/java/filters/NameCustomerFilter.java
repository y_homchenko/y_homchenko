package filters;

import model.Cart;
import org.apache.log4j.Logger;
import servlets.StartServlet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class filter checks nameCustomer is not null
 */
public class NameCustomerFilter implements Filter {
    private final Logger LOGGER = Logger.getLogger(NameCustomerFilter.class);
    private final String LOG_FILTER_NAME_CUSTOMER = "FilterNameCustomer: ";
    private final String REDIRECT_PATH_START = "/start";

    public void init(FilterConfig config) throws ServletException {
    }

    /**
     * Handles {@link Filter} doFilter Method
     *
     * @param request  - request the {@link ServletRequest}
     * @param response - response the {@link ServletResponse}
     * @param chain - chain the {@link FilterChain}
     * @throws ServletException - the Servlet exception
     * @throws IOException - the IO exception
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        final HttpSession session = ((HttpServletRequest) request).getSession();
        final Cart cart = (Cart) session.getAttribute("cart");
        if (cart == null) {
            LOGGER.error(LOG_FILTER_NAME_CUSTOMER + "cart is null");
            try {
                ((HttpServletResponse) response).sendRedirect(REDIRECT_PATH_START);
            } catch (IOException e) {
                LOGGER.error(LOG_FILTER_NAME_CUSTOMER + e);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    public void destroy() {
    }

}
