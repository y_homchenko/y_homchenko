package model;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Customer cart
 * nameCustomer - buyer's name
 * order - shopping cart
 */
public class Cart {
    private final String nameCustomer;
    private ArrayList<ConcurrentHashMap<String, Double>> order;

    private boolean checkAgree;

    public Cart(String nameCustomer) {
        this.nameCustomer = nameCustomer;
        this.checkAgree = false;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public ArrayList<ConcurrentHashMap<String, Double>> getOrder() {
        return order;
    }

    public boolean isCheckAgree() {
        return checkAgree;
    }

    public void setCheckAgree(boolean checkAgree) {
        this.checkAgree = checkAgree;
    }

    /**
     * adds to the cart the name of the item and its cost.
     * @param name - String name item
     * @param cost - Double cost item
     */
    public void putOrder(String name, Double cost) {
        if (order == null) {
            order = new ArrayList<>();
        }
        ConcurrentHashMap<String, Double> item = new ConcurrentHashMap<>();
        item.put(name, cost);
        order.add(item);
    }

    @Override
    public String toString() {
        Double total = 0.0;
        StringBuilder result = new StringBuilder();
        ArrayList<ConcurrentHashMap<String, Double>> item = getOrder();
        if (item != null) {
            for (int j = 0; j < item.size(); j++) {
                for (String key: item.get(j).keySet()) {
                    result.append("<p>").append(j+1).append(") ").append(key).append(" ").append(item.get(j).get(key)).append("$</p>");
                    total += item.get(j).get(key);
                }
            }
        }
        result.append("<h4>Total: $ ").append(total).append("</h4>");
        return result.toString();
    }
}
