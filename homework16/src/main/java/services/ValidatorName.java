package services;

/**
 * Validation
 */
public class ValidatorName {

    /**
     * Validation left and right angle bracket
     *
     * @param name - string
     * @return - string name after replacement "<" to "&lt;" and ">" to "&gt;"
     */
    public String checkingName(String name) {
        return name == null ? "" : name.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
}
