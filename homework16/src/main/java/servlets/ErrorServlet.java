package servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ErrorServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(ErrorServlet.class);
    private final String REDIRECT_PATH_START = "/start";
    private final String LOG_ERROR = "ErrorServlet: ";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_ERROR + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ERROR + "doGet");
        final String body =
                "<html>"
                        + "<head>"
                        + "<meta charset='utf8'>"
                        + "</head>"
                        + "<h1>Oops!</h1>"
                        + "<body>"
                        + "<h4>You shouldn't be here</h4>"
                        + "<h4>Please, agree with the terms of service first.</h4>"
                        + "<a href="
                        + REDIRECT_PATH_START
                        + ">Start page</a>"
                        + "</body>"
                        + "</html>";
        try {
            response.getWriter().println(body);
        } catch (IOException e) {
            LOGGER.error(LOG_ERROR + e);
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_ERROR + "destroy");
    }


}
