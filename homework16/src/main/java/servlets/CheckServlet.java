package servlets;

import model.Cart;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CheckServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(CheckServlet.class);
    private final String REDIRECT_PATH_START = "/start";
    private final String REDIRECT_PATH_ORDER = "/order";
    private final String LOG_CHECK = "CheckServlet: ";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_CHECK + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_CHECK + "doGet");
        final HttpSession session = request.getSession();
        final Cart cart = (Cart) session.getAttribute("cart");
        if (cart == null) {
            LOGGER.error(LOG_CHECK + "cart is null");
            try {
                response.sendRedirect(REDIRECT_PATH_START);
            } catch (IOException e) {
                LOGGER.error(LOG_CHECK + e);
            }
        } else if (cart.getOrder() == null) {
            LOGGER.error(LOG_CHECK + "order is null");
            try {
                response.sendRedirect(REDIRECT_PATH_ORDER);
            } catch (IOException e) {
                LOGGER.error(LOG_CHECK + e);
            }
        } else {
            final String body =
                    "<html>"
                            + "<head>"
                            + "<meta charset='utf8'>"
                            + "</head>"
                            + "<h1>Dear "
                            + cart.getNameCustomer()
                            + ", your order:</h1>"
                            + "<body>"
                            + "<h4>Make you order:</h4>"
                            + cart.toString()
                            + "</body>"
                            + "</html>";
            try {
                response.getWriter().println(body);
            } catch (IOException e) {
                LOGGER.error(LOG_CHECK + e);
            }
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_CHECK + "destroy");
    }
}
