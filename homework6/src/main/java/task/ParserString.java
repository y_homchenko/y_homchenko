package task;

import java.util.TreeSet;

import static task.CheckIsNullOrEmpty.isArrayNullOrEmpty;
import static task.CheckIsNullOrEmpty.isStringNullOrEmpty;

/**
 * The class accepts text like String and returns a string array
 */
class ParserString {
    private static final String CHAR_IN_STRING = "[^a-zA-Z]";
    private static final String WORDS_DELIMITER = "\\s+";

    /**
     * parsing the string into words and puts them in String []
     *
     * @param string - input text
     * @return - String[] of words
     */
    static String[] parsingString(String string) {
        isStringNullOrEmpty(string);
        string = string.replaceAll(CHAR_IN_STRING, " ");
        return string.toLowerCase().trim().split(WORDS_DELIMITER);
    }

    /**
     * extraction of the first letters of words
     *
     * @param arrayWords - array of words
     * @return - sorted set of first letters of words
     */
    static TreeSet<String> extractionFirstLetters(String[] arrayWords) {
        isArrayNullOrEmpty(arrayWords);
        TreeSet<String> firstLetters = new TreeSet<String>();
        for (String word : arrayWords) {
            String c = word.substring(0, 1);
            firstLetters.add(c);
        }
        return firstLetters;
    }
}
