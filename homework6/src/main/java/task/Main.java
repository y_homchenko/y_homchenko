package task;

import java.util.TreeMap;
import java.util.TreeSet;

import static task.CounterWords.countingNumberOfRepetitionsOfWords;
import static task.CounterWords.outputToConsole;
import static task.ParserString.extractionFirstLetters;
import static task.ParserString.parsingString;

/**
 * The class with a main method.
 */
public class Main {
    private static TreeMap<String, Integer> counterWords = new TreeMap<String, Integer>();
    private static TreeSet<String> firstLetter = new TreeSet<String>();

    /**
     * Main method.
     *
     * @param args command line args.
     */
    public static void main(String[] args) {
        String testString = "Once upon a time a Wolf was lapping at a spring on a hillside, "
                + "when, looking up, what should he see but a Lamb just beginning to drink a little"
                + " lower down.";
        String[] arrayWords = parsingString(testString);
        firstLetter = extractionFirstLetters(arrayWords);
        counterWords = countingNumberOfRepetitionsOfWords(arrayWords);
        outputToConsole(counterWords, firstLetter);
    }
}