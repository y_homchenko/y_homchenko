package task;

class CheckIsNullOrEmpty {
    /**
     * Checks an array for emptiness or null.
     *
     * @param string - string.
     * @throws IllegalArgumentException when array is empty or null.
     */
    static void isStringNullOrEmpty(String string) {
        if (string == null || string.length() == 0) {
            throw new IllegalArgumentException("Text is empty");
        }
    }

    /**
     * Checks an array for emptiness or null.
     *
     * @param array string array.
     * @throws IllegalArgumentException when array is empty or null.
     */
    static void isArrayNullOrEmpty(String[] array) {
        if (array == null || array.length == 0 || array[0].equals("")) {
            throw new IllegalArgumentException("Array is empty or null");
        }
    }
}
