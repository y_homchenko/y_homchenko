package task;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import static task.CheckIsNullOrEmpty.isArrayNullOrEmpty;

/**
 * The class counts the number of identical words in a string array
 */
class CounterWords {
    /**
     * Stores the number of words that occur in an array of strings.
     */
    private static TreeMap<String, Integer> counterWords = new TreeMap<String, Integer>();

    /**
     * Counts the number of identical words in a string array.
     *
     * @param array string array.
     */
    static TreeMap<String, Integer> countingNumberOfRepetitionsOfWords(String[] array) {
        isArrayNullOrEmpty(array);
        for (String string : array) {
            counterWords.put(string, counterWords.containsKey(string) ? counterWords.get(string) + 1
                    : 1);
        }
        return counterWords;
    }

    /**
     * displays all words in the console indicating how many times they appear in the text,
     * grouping them by the first letter
     *
     * @param counterWords - words and number how many times do they occur in the text
     * @param firstLetters - first lettrs of words
     */
    static void outputToConsole(TreeMap counterWords, TreeSet<String> firstLetters) {
        for (String c : firstLetters) {
            System.out.println(c.toUpperCase() + ":");
            for (Object element : counterWords.entrySet()) {
                Map.Entry mapEntry = (Map.Entry) element;
                if (mapEntry.getKey().toString().startsWith(c)) {
                    System.out.println("  " + mapEntry.getKey() + " - " + mapEntry.getValue());
                }
            }
        }
    }
}
