package task;

import org.junit.Test;

public class CheckIsNullOrEmptyTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIsStringNullOrEmptyNull() {
        CheckIsNullOrEmpty.isStringNullOrEmpty(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsStringNullOrEmpty() {
        CheckIsNullOrEmpty.isStringNullOrEmpty("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsArrayNullOrEmptyEmpty() {
        CheckIsNullOrEmpty.isArrayNullOrEmpty(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsArrayNullOrEmpty() {
        CheckIsNullOrEmpty.isArrayNullOrEmpty(new String[]{""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsArrayNullOrEmptyNull() {
        CheckIsNullOrEmpty.isArrayNullOrEmpty(null);
    }
}