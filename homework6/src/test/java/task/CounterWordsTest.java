package task;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.TreeMap;
import java.util.TreeSet;

public class CounterWordsTest {

    @Test
    public void testCountingNumberOfRepetitionsOfWords() {
        String[] arrayString = new String[]{"first", "test", "and", "test", "first"};
        TreeMap<String, Integer> expected = new TreeMap<String, Integer>() {{
            put("test", 2);
            put("first", 2);
            put("and", 1);
        }};
        TreeMap<String, Integer> actual = CounterWords.countingNumberOfRepetitionsOfWords(arrayString);
        Assert.assertEquals(expected, actual);
    }

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void testOutputToConsole() {
        String expected = "A:\r\n" +
                "  and - 1\r\n" +
                "F:\r\n" +
                "  first - 2\r\n" +
                "T:\r\n" +
                "  test - 2\r\n";
        TreeMap<String, Integer> counterWords = new TreeMap<String, Integer>() {{
            put("test", 2);
            put("first", 2);
            put("and", 1);
        }};
        TreeSet<String> firstLetters = new TreeSet<String>() {{
            add("a");
            add("f");
            add("t");
        }};
        CounterWords.outputToConsole(counterWords, firstLetters);
        Assert.assertEquals(expected, output.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

}