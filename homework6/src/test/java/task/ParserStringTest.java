package task;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.TreeSet;

public class ParserStringTest {

    @Test
    public void testParsingStringWords() {
        String[] expected = new String[] {"first", "test", "test", "first"};
        String[] actual = ParserString.parsingString("First test test first");
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testParsingStringWordsOtherChars() {
        String[] expected = new String[] {"first", "test", "test", "first"};
        String[] actual = ParserString.parsingString(" First[, '#' @test \\?&()!/ * 25; : test1 - first.]");
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testExtractionFirstLetters() {
        TreeSet<String> expected = new TreeSet<String>(Arrays.asList("t", "f"));
        TreeSet<String> actual = ParserString.extractionFirstLetters(new String[] {"first", "test", "test", "first"});
        Assert.assertEquals(expected, actual);
    }
}