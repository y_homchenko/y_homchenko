package homework2.task2;

public class Fibonacci {

    /**
     * Method Fibonacci withdrawal using a loop while
     *
     * @param n - number of displayed numbers
     * @return String fibonacci
     */
    public static String fibonacciWhile(int n) {
        int i = 2;
        int temp;
        int fib1 = 0;
        int fib2 = 1;
        String fibonacci = Integer.toString(fib1);
        while (i <= n) {
            fibonacci += " " + fib2;
            i++;
            temp = fib1;
            fib1 = fib2;
            fib2 = fib2 + temp;
        }
        return fibonacci;
    }

    /**
     * Method Fibonacci withdrawal using a loop do-while
     *
     * @param n - number of displayed numbers
     * @return String fibonacci
     */
    public static String fibonacciDoWhile(int n) {
        int i = 1;
        int temp;
        int fib1 = 0;
        int fib2 = 1;
        String fibonacci = "";
        do {
            fibonacci += fib1 + " ";
            i++;
            temp = fib1;
            fib1 = fib2;
            fib2 = fib2 + temp;
        } while (i <= n);
        return fibonacci;
    }

    /**
     * Method Fibonacci withdrawal using a loop for
     *
     * @param n - number of displayed numbers
     * @return String fibonacci
     */
    public static String fibonacciFor(int n) {
        int temp;
        int fib1 = 0;
        int fib2 = 1;
        String fibonacci = "";
        for (int i = 1; i <= n; i++) {
            fibonacci += fib1 + " ";
            temp = fib1;
            fib1 = fib2;
            fib2 = fib2 + temp;
        }
        return fibonacci;
    }

    /**
     * Method checks n <= 0 or n > 0
     *
     * @param n - Fibonacci numbers
     * @return - false if n <= 0 or true if n > 0
     */
    public static boolean isPositiveArgumentN(int n) {
        if (n <= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method for choosing the type of loop to calculate Fibonacci
     *
     * @param loopType type loop 1 - while, 2 - do-while, 3 - for
     * @param n        - n!
     */
    public static void choiceLoopTypeFibonacci(int loopType, int n) {
        switch (ChoiceLoopType.choiceLoopType(loopType)) {
            case WHILE: {
                System.out.println(fibonacciWhile(n));
                break;
            }
            case DOWHILE: {
                System.out.println(fibonacciDoWhile(n));
                break;
            }
            case FOR: {
                System.out.println(fibonacciFor(n));
                break;
            }
        }
    }
}
