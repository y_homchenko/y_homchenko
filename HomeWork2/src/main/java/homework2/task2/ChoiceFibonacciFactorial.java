package homework2.task2;

public class ChoiceFibonacciFactorial {

    /**
     * Method returns the value of enum AlgorithmId depending on the obtained value of the type of algorithm
     *
     * @param algorithmId - type of algorithm
     * @return AlgorithmId.FIBONACCI if algorithmId == 1 or AlgorithmId.FACTORIAL if algorithmId == 2
     * or throws an exception
     */
    public static AlgorithmId choiceFibonacciFactorial(int algorithmId) {
        if (algorithmId == 1) {
            return AlgorithmId.FIBONACCI;
        } else if (algorithmId == 2) {
            return AlgorithmId.FACTORIAL;
        } else {
            throw new IllegalArgumentException("Incorrect argument algorithmId");
        }
    }
}
