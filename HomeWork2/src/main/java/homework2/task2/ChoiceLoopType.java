package homework2.task2;

public class ChoiceLoopType {

    /**
     * Method returns the value of enum LoopType depending on the obtained value of the type of loopType
     *
     * @param loopType - type of algorithm
     * @return LoopType.WHILE if loopType == 1 or LoopType.DOWHILE if loopType == 2 or LoopType.FOR if loopType == 3
     * or throws an exception
     */
    public static LoopType choiceLoopType(int loopType) {
        if (loopType == 1) {
            return LoopType.WHILE;
        } else if (loopType == 2) {
            return LoopType.DOWHILE;
        } else if (loopType == 3) {
            return LoopType.FOR;
        } else {
            throw new IllegalArgumentException("Incorrect argument loopType");
        }
    }
}
