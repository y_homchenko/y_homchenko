package homework2.task2;

public class CalcFibonacciFactorial {

    /**
     * Displaying Fibonacci or Factorial Numbers using loops
     *
     * @param args Command line arguments: algorithmId, loopType, n.
     *             int algorithmId 1 - Fibonacci, 2 - Factorial Numbers.
     *             int loopType 1 - while, 2 - do-while, 3 - for.
     *             int n - parameter to calculate Fibonacci or Factorial Numbers.
     */
    public static void main(String[] args) {
        if (args.length == 3) {
            int algorithmId = parseArgInt(args[0]);
            int loopType = parseArgInt(args[1]);
            int n = parseArgInt(args[2]);
            switch (ChoiceFibonacciFactorial.choiceFibonacciFactorial(algorithmId)) {
                case FACTORIAL: {
                    if (Factorial.isNotNegativeArgumentN(n)) {
                        Factorial.choiceLoopTypeFactorial(loopType, n);
                    } else {
                        throw new IllegalArgumentException("Incorrect argument n");
                    }
                    break;
                }
                case FIBONACCI: {
                    if (Fibonacci.isPositiveArgumentN(n)) {
                        Fibonacci.choiceLoopTypeFibonacci(loopType, n);
                    } else {
                        throw new IllegalArgumentException("Incorrect argument n");
                    }
                    break;
                }
            }
        } else {
            System.out.println("Invalid number of arguments");
        }
    }

    /**
     * Method for parse argument Integer
     *
     * @param arg actual argument
     * @return parsed argument
     */
    public static int parseArgInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }
}
