package homework2.task2;

public class Factorial {

    /**
     * Method of displaying factorial using loop while
     *
     * @param n - n!
     * @return n!
     */
    public static int factorialWhile(int n) {
        int factorial = 1;
        int i = 2;
        while (i <= n) {
            factorial *= i;
            i++;
        }
        return factorial;
    }

    /**
     * Method of displaying factorial using loop do-while
     *
     * @param n - n!
     * @return n!
     */
    public static int factorialDoWhile(int n) {
        int factorial = 1;
        int i = 1;
        do {
            factorial *= i;
            i++;
        } while (i <= n);
        return factorial;
    }

    /**
     * Method of displaying factorial using loop for
     *
     * @param n - n!
     * @return n!
     */
    public static int factorialFor(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }

    /**
     * Method checks n < 0 or n >= 0
     *
     * @param n - n!
     * @return - false if n < 0 or true if n >= 0
     */
    public static boolean isNotNegativeArgumentN(int n) {
        if (n < 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method for choosing the type of loop to calculate factorial
     *
     * @param loopType type loop 1 - while, 2 - do-while, 3 - for
     * @param n        - n!
     */
    public static void choiceLoopTypeFactorial(int loopType, int n) {
        switch (ChoiceLoopType.choiceLoopType(loopType)) {
            case WHILE: {
                System.out.println(n + "! = " + factorialWhile(n));
                break;
            }
            case DOWHILE: {
                System.out.println(n + "! = " + factorialDoWhile(n));
                break;
            }
            case FOR: {
                System.out.println(n + "! = " + factorialFor(n));
                break;
            }
        }
    }
}
