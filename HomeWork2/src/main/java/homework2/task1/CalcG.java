package homework2.task1;

public class CalcG {

    /**
     * Displays the value of the number G
     * Application start point.
     *
     * @param args Command line arguments int a, int b, double m1, double m2
     */
    public static void main(String[] args) {
        if (args.length == 4) {
            int a = parseArgInt(args[0]);
            int p = parseArgInt(args[1]);
            double m1 = parseArgDouble(args[2]);
            double m2 = parseArgDouble(args[3]);
            System.out.println("G = " + calcG(a, p, m1, m2));
        } else {
            System.out.println("Invalid number of arguments");
        }
    }

    /**
     * Method for calculation G
     *
     * @param a  int
     * @param p  int
     * @param m1 double
     * @param m2 double
     */
    public static double calcG(int a, int p, double m1, double m2) {
        return ((4 * Math.pow(Math.PI, 2)) *
                Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
    }

    /**
     * Method for parse argument Integer
     *
     * @param arg actual argument
     * @return parsed argument
     */
    public static int parseArgInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }

    /**
     * Method for parse argument Double
     *
     * @param arg actual argument
     * @return parsed argument
     */
    public static double parseArgDouble(String arg) {
        try {
            return Double.parseDouble(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        } catch (NullPointerException e) {
            throw new NullPointerException("Argument is null");
        }
    }
}
