package homework2.task2;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {

    @Test
    public void testFibonacciWhile() {
        String expected = "0 1 1 2 3";
        String actual = Fibonacci.fibonacciWhile(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciWhileOne() {
        String expected = "0";
        String actual = Fibonacci.fibonacciWhile(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciWhileTwo() {
        String expected = "0 1";
        String actual = Fibonacci.fibonacciWhile(2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciWhileThree() {
        String expected = "0 1 1";
        String actual = Fibonacci.fibonacciWhile(3);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciDoWhile() {
        String expected = "0 1 1 2 3";
        String actual = Fibonacci.fibonacciDoWhile(5).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciDoWhileOne() {
        String expected = "0";
        String actual = Fibonacci.fibonacciDoWhile(1).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciDoWhileTwo() {
        String expected = "0 1";
        String actual = Fibonacci.fibonacciDoWhile(2).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciDoWhileThree() {
        String expected = "0 1 1";
        String actual = Fibonacci.fibonacciDoWhile(3).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciFor() {
        String expected = "0 1 1 2 3";
        String actual = Fibonacci.fibonacciFor(5).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciForOne() {
        String expected = "0";
        String actual = Fibonacci.fibonacciDoWhile(1).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciForTwo() {
        String expected = "0 1";
        String actual = Fibonacci.fibonacciDoWhile(2).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFibonacciForThree() {
        String expected = "0 1 1";
        String actual = Fibonacci.fibonacciDoWhile(3).trim();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsPositiveArgumentNNegative() {
        boolean expected = false;
        boolean actual = Fibonacci.isPositiveArgumentN(-1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsPositiveArgumentNZero() {
        boolean expected = false;
        boolean actual = Fibonacci.isPositiveArgumentN(0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsPositiveArgumentNPositive() {
        boolean expected = true;
        boolean actual = Fibonacci.isPositiveArgumentN(1);
        Assert.assertEquals(expected, actual);
    }
}