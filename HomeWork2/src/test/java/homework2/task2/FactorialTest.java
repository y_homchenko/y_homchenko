package homework2.task2;

import org.junit.Assert;
import org.junit.Test;

public class FactorialTest {

    @Test
    public void testFactorialWhile() {
        int expected = 120;
        int actual = Factorial.factorialWhile(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialWhileZero() {
        int expected = 1;
        int actual = Factorial.factorialWhile(0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialWhileOne() {
        int expected = 1;
        int actual = Factorial.factorialWhile(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialDoWhile() {
        int expected = 120;
        int actual = Factorial.factorialDoWhile(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialDoWhileZero() {
        int expected = 1;
        int actual = Factorial.factorialDoWhile(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialDoWhileOne() {
        int expected = 1;
        int actual = Factorial.factorialDoWhile(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialFor() {
        int expected = 120;
        int actual = Factorial.factorialFor(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialForZero() {
        int expected = 1;
        int actual = Factorial.factorialFor(0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialForOne() {
        int expected = 1;
        int actual = Factorial.factorialFor(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsNotNegativeArgumentNPositive() {
        boolean expected = true;
        boolean actual = Factorial.isNotNegativeArgumentN(5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsNotNegativeArgumentNNegative() {
        boolean expected = false;
        boolean actual = Factorial.isNotNegativeArgumentN(-5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testIsNotNegativeArgumentNZero() {
        boolean expected = true;
        boolean actual = Factorial.isNotNegativeArgumentN(0);
        Assert.assertEquals(expected, actual);
    }
}