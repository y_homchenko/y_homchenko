package homework2.task2;

import org.junit.Assert;
import org.junit.Test;

public class ChoiceLoopTypeTest {

    @Test
    public void testChoiceLoopTypeWhile() {
        LoopType expected = LoopType.WHILE;
        LoopType actual = ChoiceLoopType.choiceLoopType(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testChoiceLoopTypeDoWhile() {
        LoopType expected = LoopType.DOWHILE;
        LoopType actual = ChoiceLoopType.choiceLoopType(2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testChoiceLoopTypeFor() {
        LoopType expected = LoopType.FOR;
        LoopType actual = ChoiceLoopType.choiceLoopType(3);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChoiceLoopTypeOutOfRange() {
        ChoiceLoopType.choiceLoopType(4);
    }
}