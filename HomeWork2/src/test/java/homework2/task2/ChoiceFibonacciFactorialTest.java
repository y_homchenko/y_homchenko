package homework2.task2;

import org.junit.Assert;
import org.junit.Test;

public class ChoiceFibonacciFactorialTest {

    @Test
    public void testChoiceFibonacciFactorialFibonacci() {
        AlgorithmId expected = AlgorithmId.FIBONACCI;
        AlgorithmId actual = ChoiceFibonacciFactorial.choiceFibonacciFactorial(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testChoiceFibonacciFactorialFactorial() {
        AlgorithmId expected = AlgorithmId.FACTORIAL;
        AlgorithmId actual = ChoiceFibonacciFactorial.choiceFibonacciFactorial(2);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChoiceFibonacciFactorialOutOfRange() {
        AlgorithmId actual = ChoiceFibonacciFactorial.choiceFibonacciFactorial(3);
    }
}