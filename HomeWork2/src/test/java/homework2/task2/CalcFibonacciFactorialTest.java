package homework2.task2;

import org.junit.Assert;
import org.junit.Test;

public class CalcFibonacciFactorialTest {

    @Test
    public void testMain() {
        CalcFibonacciFactorial.main(new String[]{"1", "3", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgs() {
        CalcFibonacciFactorial.main(new String[]{"", "", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsFirst() {
        CalcFibonacciFactorial.main(new String[]{"", "3", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsSecond() {
        CalcFibonacciFactorial.main(new String[]{"1", "", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsThird() {
        CalcFibonacciFactorial.main(new String[]{"1", "3", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsOneTwo() {
        CalcFibonacciFactorial.main(new String[]{"", "", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsOneThree() {
        CalcFibonacciFactorial.main(new String[]{"", "2", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsTwoThree() {
        CalcFibonacciFactorial.main(new String[]{"1", "", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsAll() {
        CalcFibonacciFactorial.main(new String[]{"", "", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeFirst() {
        CalcFibonacciFactorial.main(new String[]{"-1", "2", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeSecond() {
        CalcFibonacciFactorial.main(new String[]{"1", "-2", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeThird() {
        CalcFibonacciFactorial.main(new String[]{"1", "2", "-5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeOneTwo() {
        CalcFibonacciFactorial.main(new String[]{"-1", "-2", "5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeOneThree() {
        CalcFibonacciFactorial.main(new String[]{"-1", "2", "-5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeTwoThree() {
        CalcFibonacciFactorial.main(new String[]{"1", "-2", "-5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsNegativeAll() {
        CalcFibonacciFactorial.main(new String[]{"-1", "-2", "-5"});
    }

    @Test
    public void testParseArgInt() {
        int expected = 3;
        int actual = CalcFibonacciFactorial.parseArgInt("3");
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseArgIntNoInt() {
        int expected = 3;
        int actual = CalcFibonacciFactorial.parseArgInt("3.0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseArgIntNoNumber() {
        int expected = 3;
        int actual = CalcFibonacciFactorial.parseArgInt("ch");
    }
}