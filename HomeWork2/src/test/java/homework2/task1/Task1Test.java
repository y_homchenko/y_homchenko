package homework2.task1;

import org.junit.Assert;
import org.junit.Test;

public class Task1Test {

    @Test
    public void testCalcG() {
        double expected = 29.283441;
        double actual = CalcG.calcG(3, 2, 3.5, 5.6);
        Assert.assertEquals(expected, actual, 0.000001);
    }

    @Test
    public void testParseArgInt() {
        int expected = 3;
        int actual = CalcG.parseArgInt("3");
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseArgIntNoInt() {
        int expected = 3;
        int actual = CalcG.parseArgInt("3.0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseArgIntNoNumber() {
        int expected = 3;
        int actual = CalcG.parseArgInt("char");
    }

    @Test
    public void testParseArgDouble() {
        double expected = 3.5;
        double actual = CalcG.parseArgDouble("3.5");
        Assert.assertEquals(expected, actual, 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseArgDoubleNoNumber() {
        double expected = 3.5;
        double actual = CalcG.parseArgDouble("Char");
    }

    @Test
    public void testMain() {
        CalcG.main(new String[]{"3", "2", "3.5", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgOne() {
        CalcG.main(new String[]{"", "2", "3.5", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgTwo() {
        CalcG.main(new String[]{"3", "", "3.5", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgThree() {
        CalcG.main(new String[]{"3", "2", "", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgFour() {
        CalcG.main(new String[]{"3", "2", "3.5", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgs() {
        CalcG.main(new String[]{"", "", "", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsOneTwo() {
        CalcG.main(new String[]{"", "", "3.5", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsOneThree() {
        CalcG.main(new String[]{"", "2", "", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsOneFour() {
        CalcG.main(new String[]{"", "2", "3.5", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsTwoThree() {
        CalcG.main(new String[]{"3", "", "", "5.6"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsTwoFour() {
        CalcG.main(new String[]{"3", "", "3.5", ""});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainNoArgsThreeFour() {
        CalcG.main(new String[]{"3", "2", "", ""});
    }

    @Test
    public void testMainFiveArgs() {
        CalcG.main(new String[]{"3", "2", "3.5", "5.6", "10"});
    }
}