package task;

import org.junit.Test;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.After;
import org.junit.Before;

import java.io.OutputStream;
import java.net.InetSocketAddress;

import static org.junit.Assert.assertEquals;

public class ConnectionHttpClientTest {

    private static final String URL = "http://localhost:9000/1";
    private HttpServer server;

    @Before
    public void setUp() throws Exception {
        server = HttpServer.create(new InetSocketAddress(9000), 0);
        server.createContext("/1", new TestHttpHandler());
        server.setExecutor(null);
        server.start();
    }

    @After
    public void tearDown() throws Exception {
        server.stop(0);
    }

    @Test
    public void testDoGetResponseServer() throws IOException {
        ConnectionHttpClient connectionHttpClient = new ConnectionHttpClient();
        String excepted = "response";
        String actual = connectionHttpClient.doGet(URL);
        assertEquals(excepted, actual);
    }

    @Test
    public void testDoPost() throws IOException {
        Publication article = new Publication(1, 1, "SOME TITLE", "Some message");
        ConnectionHttpClient connectionHttpClient = new ConnectionHttpClient();
        Converter converter = new Converter();
        String excepted = "response";
        String actual = connectionHttpClient.doPost(URL, "response");
        assertEquals(excepted, actual);
    }

    class TestHttpHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = "response";
            if (httpExchange.getRequestMethod().equals("GET")) {
                httpExchange.sendResponseHeaders(200, response.length());
            } else {
                httpExchange.sendResponseHeaders(201, response.length());
            }
            try (OutputStream os = httpExchange.getResponseBody()) {
                os.write(response.getBytes());
            }
        }
    }
}
