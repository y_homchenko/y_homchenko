package task;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {

    @Mock
    Publication mockArticle;
    @Mock
    Converter mockConverter;
    @Mock
    PrintStream stream;

    @Test
    public void testPublicationPrint() throws JsonProcessingException {
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        System.setOut(stream);
        when(mockConverter.toJavaObject("OK")).thenReturn(mockArticle);
        Main.publicationPrint("OK", mockConverter);
        verify(mockConverter).toJavaObject("OK");
        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), "mockArticle");
    }
}
