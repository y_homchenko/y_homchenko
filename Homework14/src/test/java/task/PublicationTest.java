package task;

import org.junit.Assert;
import org.junit.Test;

public class PublicationTest {

    @Test
    public void testGetUserId() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        int excepted = 1;
        int actual = article.getUserId();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testSetUserId() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        int excepted = 10;
        article.setUserId(10);
        int actual = article.getUserId();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testGetId() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        int excepted = 101;
        int actual = article.getId();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testSetId() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        int excepted = 102;
        article.setUserId(102);
        int actual = article.getUserId();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testGetTitle() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        String excepted = "SOME TITLE";
        String actual = article.getTitle();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testSetTitle() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        String excepted = "TEST TITLE";
        article.setTitle("TEST TITLE");
        String actual = article.getTitle();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testGetBody() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        String excepted = "Some message";
        String actual = article.getBody();
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testSetBody() {
        Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
        String excepted = "TEST message";
        article.setBody("TEST message");
        String actual = article.getBody();
        Assert.assertEquals(excepted, actual);
    }
}
