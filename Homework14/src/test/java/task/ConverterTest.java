package task;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Test;

public class ConverterTest {

    private Publication article = new Publication(1, 101, "SOME TITLE", "Some message");
    private Converter converter = new Converter();

    @Test
    public void testToJSON() throws JsonProcessingException {
        String excepted = "{\"userId\":1,\"id\":101,\"title\":\"SOME TITLE\",\"body\":\"Some message\"}";
        String actual = converter.toJSON(article);
        Assert.assertEquals(excepted, actual);
    }

    @Test
    public void testToJavaObject() throws JsonProcessingException {
        Publication excepted = new Publication(1, 101, "TITLE", "Message");
        Publication actual = converter.toJavaObject("{\"userId\":1,\"id\":101,\"title\":\"TITLE\",\"body\":\"Message\"}");
        Assert.assertEquals(excepted, actual);
    }
}
