package task;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Implementation of GET and POST methods using HttpHttpURLConnection.
 */
class ConnectionHttpURL implements Connection {

    private static final Logger log = Logger.getLogger(ConnectionHttpURL.class);
    private static final int CONNECTION_TIMEOUT = 6000;

    /**
     * Method GET.
     *
     * @param url - string url
     * @return - string response or string empty
     */
    public String doGet(String url) {
        StringBuilder respBody = new StringBuilder();
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setReadTimeout(CONNECTION_TIMEOUT);
            connection.connect();
            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                log.info("GET connection established");
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                in.lines().forEach(respBody::append);
                in.close();
            } else {
                log.error("GET fail: " + connection.getResponseCode() + ", " + connection.getResponseMessage());
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("GET" + e);
        } finally {
            if (connection != null) {
                connection.disconnect();
                log.info("GET connection disconnected");
            }
        }
        log.info("GET return");
        return respBody.toString();
    }

    /**
     * Method POST.
     *
     * @param url         - string url
     * @param bodyRequest - string request body
     * @return - string response
     */
    public String doPost(String url, String bodyRequest) {
        String content = "";
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setUseCaches(false);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setReadTimeout(CONNECTION_TIMEOUT);
            try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
                out.writeBytes(bodyRequest);
                log.info("POST write data");
            } catch (final UnsupportedEncodingException e) {
                log.error("POST " + e);
                e.printStackTrace();
            }
            content = readInputStream(connection);
            log.info("POST read data");
        } catch (IOException e) {
            log.error("POST " + e);
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
                log.info("POST connection disconnected");
            }
        }
        log.info("POST return");
        return content;
    }

    /**
     * Method read response
     *
     * @param con - instance HttpURLConnection
     * @return - string response
     */
    private String readInputStream(HttpURLConnection con) {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            log.info("readInputStream return");
            return content.toString();
        } catch (Exception e) {
            log.error("readInputStream " + e);
            e.printStackTrace();
            log.info("readInputStream return");
            return "Read is fail!";
        }
    }
}
