package task;

import java.io.IOException;

/**
 * Interface methods GET and POST.
 */
public interface Connection {
    String doPost(String url, String bodyRequest) throws IOException;

    String doGet(String url) throws IOException;
}
