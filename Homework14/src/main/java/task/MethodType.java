package task;

/**
 * Enumeration of possible methods of accessing the server.
 */
public enum MethodType {
    GET_HTTPURLCONNECTION, POST_HTTPURLCONNECTION, GET_HTTPCLIENT, POST_HTTPCLIENT
}
