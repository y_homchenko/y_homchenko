package task;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Implementation of GET and POST methods using HttpClient.
 */
class ConnectionHttpClient implements Connection {
    private static final Logger log = Logger.getLogger(ConnectionHttpClient.class);

    /**
     * Method GET.
     *
     * @param url - string url
     * @return - string response or string empty
     * @throws IOException - exception
     */
    public String doGet(String url) throws IOException {
        String result = "";
        HttpUriRequest request = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            HttpEntity respBody = response.getEntity();
            if (respBody != null) {
                log.info("GET read data");
                result = EntityUtils.toString(respBody);
            }
        } catch (IOException e) {
            log.error("GET " + e);
            e.printStackTrace();
        }
        log.info("GET return");
        return result;
    }

    /**
     * Method POST.
     *
     * @param url         - string url
     * @param bodyRequest - string request body
     * @return - string response
     * @throws IOException - exception
     */
    public String doPost(String url, String bodyRequest) throws IOException {
        String content = "";
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("content-type", "application/json");
        httpPost.setEntity(new StringEntity(bodyRequest));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpPost)) {
            log.info("POST read data");
            content = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            log.error("POST " + e);
            e.printStackTrace();
        }
        log.info("POST return");
        return content;
    }
}
