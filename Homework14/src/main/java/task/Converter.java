package task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Converts an object in JSON and JSON into an object using Jackson.
 */
class Converter {

    Converter() {
    }

    /**
     * Method converts instance Publication to JSON.
     *
     * @param user - instance Publication
     * @return - string JSON
     * @throws JsonProcessingException - exception
     */
    String toJSON(Publication user) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(user);
    }

    /**
     * Method converts string JSON to instance Publication.
     *
     * @param stringJson - string JSON
     * @return - instance Publication
     * @throws JsonProcessingException - exception
     */
    Publication toJavaObject(String stringJson) throws JsonProcessingException {
        if (stringJson != null) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(stringJson, Publication.class);
        }
        return null;
    }
}
