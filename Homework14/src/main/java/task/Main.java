package task;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;

import java.io.IOException;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class);
    private static int id = 0;
    private static final String ARGUMENTS =
            "Arguments: [method(GET or POST)] [number of id-article] " +
                    "[typeClass(1 - HttpURLConnection or 2 - HttpClient)]";
    private static final String URL = "https://jsonplaceholder.typicode.com/posts";

    public static void main(String[] args) throws IOException {
        Connection connection;
        Converter converter = new Converter();
        String stringBody;
        Publication article = new Publication(1, id, "SOME TITLE", "Some message");

        MethodType methodType = chooseMethod(args);
        // HttpURLConnection GET
        if (methodType == MethodType.GET_HTTPURLCONNECTION) {
            connection = new ConnectionHttpURL();
            stringBody = connection.doGet(URL + "/" + id);
            publicationPrint(stringBody, converter);
        }
        // HttpURLConnection POST
        if (methodType == MethodType.POST_HTTPURLCONNECTION) {
            connection = new ConnectionHttpURL();
            stringBody = converter.toJSON(article);
            stringBody = connection.doPost(URL, stringBody);
            publicationPrint(stringBody, converter);
        }
        //HttpClient GET
        if (methodType == MethodType.GET_HTTPCLIENT) {
            connection = new ConnectionHttpClient();
            stringBody = connection.doGet(URL + "/" + id);
            publicationPrint(stringBody, converter);
        }
        //HttpClient POST
        if (methodType == MethodType.POST_HTTPCLIENT) {
            connection = new ConnectionHttpClient();
            stringBody = converter.toJSON(article);
            stringBody = connection.doPost(URL, stringBody);
            publicationPrint(stringBody, converter);
        }
    }

    /**
     * Print to console.
     *
     * @param stringBody - string request/response body
     * @param converter  - instance Converter
     * @throws JsonProcessingException - exception
     */
    static void publicationPrint(String stringBody, Converter converter) throws JsonProcessingException {
        Publication publication = converter.toJavaObject(stringBody);
        String result;
        if (publication != null) {
            publication.setId(id);
            result = publication.toString();
            System.out.println(result);
            log.info("Console output: " + result);
        }
    }

    /**
     * Data processing selection method.
     *
     * @param args - command line parametrs
     * @return - int type of method
     */
    private static MethodType chooseMethod(String[] args) {
        MethodType result = null;
        String method;
        int typeClass;
        if (args.length != 3) {
            log.error("Invalid number of arguments!");
            throw new IllegalArgumentException("Invalid number of arguments!\n" + ARGUMENTS);
        }
        if (args[0].equalsIgnoreCase("GET") || args[0].equalsIgnoreCase("POST")) {
            method = args[0];
        } else {
            log.error("Illegal first argument!");
            throw new IllegalArgumentException("Illegal first argument!\n" + ARGUMENTS);
        }
        try {
            id = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            log.error("Illegal second argument! " + e);
            throw new IllegalArgumentException("Illegal second argument!\n" + ARGUMENTS + "\n" + e);
        }
        try {
            typeClass = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            log.error("The third argument is not number! " + e);
            throw new IllegalArgumentException("The third argument is not number!\n" + ARGUMENTS + "\n" + e);
        }
        if (typeClass == 1 && method.equalsIgnoreCase("GET")) {
            result = MethodType.GET_HTTPURLCONNECTION;
            log.info(result);
        }
        if (typeClass == 1 && method.equalsIgnoreCase("POST")) {
            result = MethodType.POST_HTTPURLCONNECTION;
            log.info(result);
        }
        if (typeClass == 2 && method.equalsIgnoreCase("GET")) {
            result = MethodType.GET_HTTPCLIENT;
            log.info(result);
        }
        if (typeClass == 2 && method.equalsIgnoreCase("POST")) {
            result = MethodType.POST_HTTPCLIENT;
            log.info(result);
        }
        return result;
    }
}
