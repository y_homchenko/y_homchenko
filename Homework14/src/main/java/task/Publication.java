package task;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Class publication.
 */
public class Publication {
    @JsonProperty("userId")
    private int userId;
    @JsonProperty("id")
    private int id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("body")
    private String body;

    /**
     * Constructor.
     */
    Publication() {
    }

    /**
     * Constructor.
     *
     * @param userId - userID
     * @param id     - id
     * @param title  - title
     * @param body   - body
     */
    Publication(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    /**
     * Method returns the value of userId.
     *
     * @return - int value of userId
     */
    int getUserId() {
        return userId;
    }

    /**
     * Method sets the value of userId.
     *
     * @param userId - int value of userId
     */
    void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Method returns the value of id.
     *
     * @return - int value of id
     */
    int getId() {
        return id;
    }

    /**
     * Method sets the value of id.
     *
     * @param id - int value of id
     */
    void setId(int id) {
        this.id = id;
    }

    /**
     * Method returns the value of title.
     *
     * @return - string value of title
     */
    String getTitle() {
        return title;
    }

    /**
     * Method sets the value of title.
     *
     * @param title - string value of title
     */
    void setTitle(String title) {
        this.title = title;
    }

    /**
     * Method returns the value of body.
     *
     * @return - string value of body
     */
    String getBody() {
        return body;
    }

    /**
     * Method sets the value of title.
     *
     * @param body - string value of body
     */
    void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Article [" + id + "]: " +
                "User [" + userId + "] " +
                "Title [" + title + "] " +
                "Message [" + body + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return userId == that.userId &&
                id == that.id &&
                title.equals(that.title) &&
                body.equals(that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, id, title, body);
    }
}
