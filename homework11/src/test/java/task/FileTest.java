package task;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class FileTest {
    public static Set<String> sets(String... strings) {
        HashSet<String> set = new HashSet<String>();
        for (String s : strings) {
            set.add(s);
        }
        return set;
    }

    Set<String> stringSet = sets("root/", "file.txt");

    @Test
    public void testGetName() {
        File file = new File("file.txt", stringSet);
        String expected = "file.txt";
        String actual = file.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAdd() {
        File file = new File("file.txt", stringSet);
        file.add(file);
    }

    @Test
    public void testEquals() {
        boolean expected = true;
        boolean actual = stringSet.equals(stringSet);
        Assert.assertEquals(expected, actual);
    }
}