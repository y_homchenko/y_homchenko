package task;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class FolderTest {

    public static Set<String> sets(String... strings) {
        HashSet<String> set = new HashSet<String>();
        for (String s : strings) {
            set.add(s);
        }
        return set;
    }

    Set<String> stringSet = sets("root/");

    @Test
    public void testGetName() {
        Folder folder = new Folder("folder/", null);
        String expected = "folder/";
        String actual = folder.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testEquals() {
        boolean expected = true;
        boolean actual = stringSet.equals(stringSet);
        Assert.assertEquals(expected, actual);
    }

}