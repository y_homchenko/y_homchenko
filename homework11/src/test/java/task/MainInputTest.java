package task;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static task.InputParse.inputProcessing;

public class MainInputTest {


    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    private Component tree = new Folder("root/", null);

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void testMainOnlyFile() {
        String expected = "root/\r\n" +
                "  file.txt\r\n";
        String enterPath = "root/file.txt";
        inputProcessing(enterPath, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }

    @Test
    public void testMainOnlyFolder() {
        String expected = "root/\r\n" +
                "  folder/\r\n";
        String enterPath = "root/folder/";
        inputProcessing(enterPath, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }

    @Test
    public void testMainOnlyFileSlash() {
        String expected = "root/\r\n" +
                "  file.txt\r\n";
        String enterPath = "root\\file.txt";
        inputProcessing(enterPath, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }

    @Test
    public void testMainOnlyFolderSlash() {
        String expected = "root/\r\n" +
                "  folder/\r\n";
        String enterPath = "root\\folder\\";
        inputProcessing(enterPath, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }


    @Test
    public void testMainFolderFile() {
        String expected = "root/\r\n" +
                "  folder1/\r\n" +
                "    file.txt\r\n";
        String enterPath = "root/folder1/file.txt";
        inputProcessing(enterPath, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }

    @Test
    public void testMainFoldersFiles() {
        String expected = "root/\r\n" +
                "  folder1/\r\n" +
                "    file.txt\r\n" +
                "  folder2/\r\n" +
                "    file1.txt\r\n";
        String enterPath = "root/folder1/file.txt";
        inputProcessing(enterPath, tree);
        String enterPathTwo = "root/folder2/file1.txt";
        inputProcessing(enterPathTwo, tree);
        tree.displayComponentInfo();
        Assert.assertEquals(expected, output.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }
}