package task;

import org.junit.Assert;
import org.junit.Test;

public class ComponentTest {


    @Test
    public void testLoad() {
        Component tree = new Folder("root/", null);
        Component treeNew = new Folder("root/", null);
        Folder folder = new Folder("folder/", null);
        tree.save(folder, "ser.dat");
        treeNew = treeNew.load("ser.dat");
        String expected = "folder/";
        String actual = treeNew.getName();
        Assert.assertEquals(expected, actual);
    }
}