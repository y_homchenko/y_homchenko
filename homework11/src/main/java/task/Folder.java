package task;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Type objects Folder.
 * Object Folder may contain other objects of type Folder and File.
 */
public class Folder extends Component {

    private String name;
    private Set<String> parentFolder;

    public Folder(String name, Set parentFolder) {
        this.name = name;
        this.parentFolder = new HashSet<>();
    }

    private Set<Component> components = new LinkedHashSet<>();

    /**
     * Get value name.
     *
     * @return variable value name.
     */
    public String getName() {
        return name;
    }

    /**
     * Adds the object File or Folder.
     *
     * @param c
     */
    @Override
    public void add(Component c) {
        components.add(c);
    }

    /**
     * Console output.
     */
    @Override
    public void displayComponentInfo() {
        System.out.println(getName());
        for (Component c : components) {
            System.out.println(c.getName());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return name.equals(folder.name) &&
                parentFolder.equals(folder.parentFolder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parentFolder);
    }
}
