package task;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Class contains methods for processing an input string.
 */
class InputParse {

    /**
     * Method adds folders and files to the tree.
     *
     * @param enterPath - line containing the path to files and folders.
     * @param tree      - folder and file tree.
     */
    static void inputProcessing(String enterPath, Component tree) {
        LinkedHashSet<Component> folderName = parseEnterPath(enterPath);
        for (Component component : folderName) {
            tree.add(component);
        }
    }

    /**
     * Parse input string to folder and file names.
     *
     * @param enterPath -line containing the path to files and folders.
     * @return - set consisting of folder names and file converted to be added to the tree.
     */
    private static LinkedHashSet<Component> parseEnterPath(String enterPath) {
        String[] tokens = enterPath.split("[\\\\|/]");
        String filename = "";
        LinkedHashSet<Component> folderName = new LinkedHashSet<>();
        if ((tokens.length != 0) && (tokens[tokens.length - 1].contains("."))) {
            String repeated = new String(new char[tokens.length - 1]).replace("\0", "  ");
            filename = repeated + tokens[tokens.length - 1];
            Set<String> parentFolderFile = new HashSet<>();
            for (int j = 0; j < tokens.length - 2; j++) {
                parentFolderFile.add(tokens[j]);
            }
            File file = new File(filename, parentFolderFile);
            for (int i = 1; i < tokens.length - 1; i++) {
                repeated = new String(new char[i]).replace("\0", "  ");
                Set<String> parentFolder = new HashSet<>();
                for (int j = 0; j < i; j++) {
                    parentFolder.add(tokens[j]);
                }
                Folder folder = new Folder(repeated + tokens[i] + "/", parentFolder);
                folderName.add(folder);
            }
            folderName.add(file);
        } else if (tokens.length != 0) {
            for (int i = 1; i < tokens.length; i++) {
                String repeated = new String(new char[i]).replace("\0", "  ");
                Set<String> parentFolder = new HashSet<String>();
                for (int j = 0; j < i; j++) {
                    parentFolder.add(tokens[j]);
                    Folder folder = new Folder(repeated + tokens[i] + "/", parentFolder);
                    folderName.add(folder);
                }
            }
        }
        return folderName;
    }
}
