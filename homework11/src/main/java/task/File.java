package task;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class File extends Component {

    private String name;
    private Set<String> parentFolder;

    File(String name, Set parentFolder) {
        this.name = name;
        this.parentFolder = new HashSet<>();
    }

    /**
     * Get value name.
     *
     * @return variable value name.
     */
    public String getName() {
        return name;
    }

    /**
     * Console output.
     */
    @Override
    public void displayComponentInfo() {
        System.out.println(getName());
    }

    /**
     * You cannot add other objects to object File.
     *
     * @param c
     */
    @Override
    public void add(Component c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return name.equals(file.name) &&
                parentFolder.equals(file.parentFolder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parentFolder);
    }
}
