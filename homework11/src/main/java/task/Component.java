package task;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

abstract class Component implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * Get name.
     * @return String name.
     */
    abstract String getName();

    /**
     * Adds object.
     * @param c
     */
    abstract void add(Component c);

    /**
     * Console output.
     */
    abstract void displayComponentInfo();

    /**
     * save serialization object
     *
     * @param tree serialization object.
     */
    void save(Component tree, String fileName) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))) {
            out.writeObject(tree);
        } catch (IOException e) {
            System.out.println("File cannot be opened." + e);
        }
    }

    /**
     * load serialization object
     *
     * @return serialization object
     */
    Component load(String fileName) {
        Component tree = new Folder("root/", null);
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            tree = (Component) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return tree;
    }
}
