package task;

import java.io.IOException;
import java.util.Scanner;

import static task.InputParse.inputProcessing;

/**
 * main user input
 */
public class MainInput {
    /**
     * file name for storing serialization data
     */
    private static final String FILE_NAME = "ser.dat";


    /**
     * User input line "path to file / directory" or Exit or Print.
     * @param args
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Component tree = new Folder("root/", null);
        boolean exitLoop = true;
        int choice;
        String enterPath;
        Scanner scanner = new Scanner(System.in);
        while (exitLoop) {
            System.out.println("Exit / Print / Save / Load");
            enterPath = scanner.next();
            if (enterPath.equalsIgnoreCase("exit")) {
                exitLoop = false;
            } else if (enterPath.equalsIgnoreCase("print")) {
                tree.displayComponentInfo();
            } else if (enterPath.equalsIgnoreCase("save")){
                tree.save(tree, FILE_NAME);
            } else if (enterPath.equalsIgnoreCase("load")){
                tree = tree.load(FILE_NAME);
            } else {
                inputProcessing(enterPath, tree);
            }
        }
    }
}
