package task;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {

    @Test
    public void testGetInstanceNull() {
        Card card = null;
        card = Card.getInstance();
        Assert.assertTrue(card != null);
    }

    @Test
    public void testGetInstance() {
        Card card = Card.getInstance();
        Card expected = card;
        Card actual = Card.getInstance();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetAccount() {
        Card card = Card.getInstance();
        int expected = 500;
        int actual = card.getAccount();
        Assert.assertEquals(expected, actual);
    }
}