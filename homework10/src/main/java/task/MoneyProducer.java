package task;

public class MoneyProducer implements Runnable {

    /**
     * Link to the account.
     * It is used for the simultaneous provision of an account support.
     */
    private Card account = Card.getInstance();

    /**
     * Amount of money to add.
     */
    private static final int ADD_AMOUNT = 10;

    /**
     * Number of milliseconds to suspend a thread.
     */
    private static final int THREAD_SLEEP_MS = 2000;

    /**
     * Maximum account value.
     */
    private static final int MAX_ACCOUNT_VALUE = 1000;

    /**
     * Minimum account value.
     */
    private static final int MIN_ACCOUNT_VALUE = 0;

    @Override
    public void run() {
        while (true) {
            final int previousAccountValue = account.getAccount();
            if (previousAccountValue >= MAX_ACCOUNT_VALUE || previousAccountValue == MIN_ACCOUNT_VALUE) {
                System.out.println("Account balance = " + account.getAccount());
                return;
            }
            Card.getInstance().addMoney(ADD_AMOUNT);
            try {
                Thread.sleep(THREAD_SLEEP_MS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
