package task;

final class Card {
    /**
     * Start account value.
     */
    private static final int START_ACCOUNT_VALUE = 500;

    /**
     * class Card one instance for all users.
     * The instance variable holds a reference to a single instance of this class.
     */
    private static Card instance;

    /**
     * Amount of money in the account.
     */
    private volatile int account;

    /**
     * Account initialization.
     */
    private Card() {
        this.account = START_ACCOUNT_VALUE;
    }

    /**
     * Returns the link to the account.
     * At the first appeal creates a single copy of the account.
     *
     * @return link to card.
     */
    static Card getInstance() {
        if (instance == null) {
            instance = new Card();
        }
        return instance;
    }

    /**
     * Withdraws the indicated amount of money from the account.
     *
     * @param amount How much money do we withdraw.
     */
    synchronized void withdrawMoney(final int amount) {
        final int previousAccountValue = account;
        account -= amount;
        System.out.println("Withdrawal made from an ATM " + Thread.currentThread().getName() + " - " +
                " Account balance before withdrawal:  " + previousAccountValue + ", after withdrawal: " +
                account);
    }

    /**
     * How much money is now in the account.
     *
     * @return Amount of money.
     */
    synchronized int getAccount() {
        return account;
    }

    /**
     * Replenishment the account.
     *
     * @param amount How much money to add.
     */
    synchronized void addMoney(final int amount) {
        final int previousAccountValue = account;
        account += amount;
        System.out.println("Add made from an ATM " + Thread.currentThread().getName() + " - " +
                " Account balance before adding:  " + previousAccountValue + ", after adding: " +
                account);
    }
}
