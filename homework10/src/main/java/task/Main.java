package task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private static final int POOL_SIZE = 10 ;

    public static void main(String[] args) {
        runWithExecutors();
    }

        private static void runWithExecutors() {
        ExecutorService executorService = Executors.newFixedThreadPool(POOL_SIZE);
        for (int i = 0; i < 5; i++) {
            executorService.execute(new MoneyProducer());
            executorService.execute(new MoneyConsumer());
        }
        executorService.shutdown();
    }
}