package task2;

import java.util.Arrays;

/**
 * utility class to calculate the median of the int array and double array.
 */
public final class Median {

    // Suppresses default constructor, ensuring non-instantiability.
    private Median() {
    }

    /**
     * Method to calculate the median of the int array
     *
     * @param array - int[]
     * @return - float median or IllegalArgumentException
     */
    public static float median(int[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Incorrect argument int[] array");
        }
        int[] copyArray = array.clone();
        Arrays.sort(copyArray);
        if (copyArray.length % 2 == 0) {
            return (copyArray[copyArray.length / 2] + copyArray[copyArray.length / 2 - 1]) / 2F;
        }
        return ((float) copyArray[copyArray.length / 2]);
    }

    /**
     * Method to calculate the median of the double array
     *
     * @param array - double[]
     * @return - double median or IllegalArgumentException
     */
    public static double median(double[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Incorrect argument int[] array");
        }
        double[] copyArray = array.clone();
        Arrays.sort(copyArray);
        if (copyArray.length % 2 == 0) {
            return (copyArray[copyArray.length / 2] + copyArray[copyArray.length / 2 - 1]) / 2;
        }
        return ((double) copyArray[copyArray.length / 2]);
    }
}
