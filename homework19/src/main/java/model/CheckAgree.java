package model;

public class CheckAgree {
    private boolean checkAgree;

    public CheckAgree() {
    }

    public boolean isCheckAgree() {
        return checkAgree;
    }

    public void setCheckAgree(boolean checkAgree) {
        this.checkAgree = checkAgree;
    }
}
