package servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ErrorServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(ErrorServlet.class);
    private final String ERROR_PATH = "/WEB-INF/view/errorPage.jsp";
    private final String LOG_ERROR = "ErrorServlet: ";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_ERROR + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ERROR + "doGet");
        try {
            request.getRequestDispatcher(ERROR_PATH).forward(request, response);
        } catch (ServletException | IOException e) {
            LOGGER.error(LOG_ERROR + "doGet ERROR");
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_ERROR + "destroy");
    }
}
