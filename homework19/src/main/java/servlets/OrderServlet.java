package servlets;

import model.CustomerOrder;
import model.Customers;
import model.OrdersGoods;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.CustomersOrdersService;
import service.GoodsService;
import service.OrdersGoodsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class OrderServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(OrderServlet.class);
    private final String REDIRECT_PATH = "/check";
    private final String ORDER_PATH = "/WEB-INF/view/orderPage.jsp";
    private final String ORDER_SERVLET_PATH = "/order";
    private final String LOG_ORDER = "OrderServlet: ";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_ORDER + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ORDER + "doGet");
        try {
            request.getRequestDispatcher(ORDER_PATH).forward(request, response);
        } catch (ServletException | IOException e) {
            LOGGER.error(LOG_ORDER + "doGet ERROR");
            e.printStackTrace();
        }
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ORDER + "doPost");
        final HttpSession session = request.getSession();
        final String item = request.getParameter("goods");
        if (request.getParameter("submit") != null) {
            try {
                response.sendRedirect(REDIRECT_PATH);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (item != null) {
                String[] str = item.split(" ");
                String title = str[0];
                double price = 0d;
                final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
                final GoodsService goodsService = context.getBean("goodsService", GoodsService.class);
                final OrdersGoodsService ordersGoodsService = context.getBean("ordersGoodsService", OrdersGoodsService.class);
                final OrdersGoods ordersGoods = context.getBean("ordersGoods", OrdersGoods.class);
                int goodId = 0;
                try {
                    price = Double.parseDouble(str[1].replace("$", ""));
                } catch (NumberFormatException e) {
                    LOGGER.error("NumberFormatException");
                    e.printStackTrace();
                }
                final Customers customer = (Customers) session.getAttribute("customer");
                int userId = customer.getId();
                final CustomersOrdersService customersOrdersService = context.getBean("customersOrdersService", CustomersOrdersService.class);
                final CustomerOrder customerOrder = customersOrdersService.findByUserId(userId);
                int orderId = customerOrder.getId();
                Double totalPrice = customerOrder.getTotalPrice();
                totalPrice += price;
                customerOrder.setTotalPrice(totalPrice);
                customersOrdersService.update(customerOrder);
                goodId = goodsService.findByTitle(title).getId();
                ordersGoods.setGoodId(goodId);
                ordersGoods.setOrderId(orderId);
                ordersGoodsService.add(ordersGoods);
                try {
                    response.sendRedirect(ORDER_SERVLET_PATH);
                } catch (IOException e) {
                    LOGGER.error("IOException");
                    e.printStackTrace();
                }
            } else {
                LOGGER.info(LOG_ORDER + "item is null");
                try {
                    response.sendRedirect(ORDER_SERVLET_PATH);
                } catch (IOException e) {
                    LOGGER.error("IOException");
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_ORDER + "destroy");
    }
}