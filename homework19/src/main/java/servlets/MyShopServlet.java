package servlets;

import model.CheckAgree;
import model.Customers;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.CustomersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class servlet start page
 */
public class MyShopServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(MyShopServlet.class);
    private final String REDIRECT_PATH = "/order";
    private final String LOG_START = "StartServlet: ";
    private final String START_PATH = "/WEB-INF/view/startPage.jsp";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_START + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_START + "doGet");
        try {
            request.getRequestDispatcher(START_PATH).forward(request, response);
        } catch (ServletException | IOException e) {
            LOGGER.error(LOG_START + "doGet ERROR");
            e.printStackTrace();
        }
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_START + "doPost");
        final HttpSession session = request.getSession();
        String name = request.getParameter("nameCustomer");
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Customers customer = context.getBean("customers", Customers.class);
        customer.setLogin(name);
        final CustomersService customerService = context.getBean("customersService", CustomersService.class);
        final CheckAgree checkAgree = context.getBean("checkAgree", CheckAgree.class);
        if (customerService.findByName(name) != null) {
            if (request.getParameter("checkBoxAgree") != null) {
                checkAgree.setCheckAgree(true);
                customerService.add(customer);
                customer = customerService.findByName(name);
                session.setAttribute("customer", customer);
            }
            session.setAttribute("checkAgree", checkAgree);
            try {
                request.getRequestDispatcher(REDIRECT_PATH).forward(request, response);
            } catch (ServletException | IOException e) {
                LOGGER.info(LOG_START + "doPost ERROR");
                e.printStackTrace();
            }
        } else {
            LOGGER.info(LOG_START + "such name already exists");
            try {
                request.getRequestDispatcher(START_PATH).forward(request, response);
            } catch (ServletException | IOException e) {
                LOGGER.info(LOG_START + "doPost ERROR");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_START + "destroy");
    }
}
