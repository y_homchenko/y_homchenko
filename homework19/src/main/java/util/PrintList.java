package util;

import model.Goods;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.GoodsService;
import service.OrdersGoodsService;

import java.util.List;

/**
 * List to String.
 */
public class PrintList {

    /**
     * List Goods to String.
     *
     * @return -String Goods
     */
    public String listGoodsToString() {
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        String result = "";
        final GoodsService goodService = context.getBean("goodsService", GoodsService.class);
        final List<Goods> goodList = goodService.getAll();
        for (Goods g : goodList) {
            result += "<option>" + g + "</option>";
        }
        return result;
    }

    /**
     * List OrdersGoods with Goods to String.
     *
     * @param id - int ID OrdersGoods
     * @return - String selected items Goods with total_price
     */
    public String listOrdersGoodsWithGoodsToString(final int id) {
        final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        StringBuilder result = new StringBuilder();
        Double total = 0.0;
        final OrdersGoodsService ordersGoodsService = context.getBean("ordersGoodsService", OrdersGoodsService.class);
        final GoodsService goodService = context.getBean("goodsService", GoodsService.class);
        final List<Integer> orderGoods = ordersGoodsService.findByOrderId(id);
        for (int i = 0; i < orderGoods.size(); i++) {
            result.append("<p>").append(i + 1).append(") ").append(goodService.findById(orderGoods.get(i)).getTitle()).append(" ").append(goodService.findById(orderGoods.get(i)).getPrice()).append("$</p>");
            total += goodService.findById(orderGoods.get(i)).getPrice();
        }
        result.append("<h4>Total: $ ").append(total).append("</h4>");
        return result.toString();
    }
}
