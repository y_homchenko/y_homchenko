package dao;

import model.Customers;

public interface CustomersDAO {

    void add(Customers customer);

    Customers findByName(String name);
}
