package dao;

import model.Goods;

import java.util.List;

public interface GoodsDAO {

    void add(Goods good);

    List<Goods> getAll();

    Goods findByTitle(String title);

    Goods findById(int id);

}
