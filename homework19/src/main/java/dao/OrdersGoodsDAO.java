package dao;

import model.OrdersGoods;

import java.util.List;

public interface OrdersGoodsDAO {

    void add(OrdersGoods orderGood);

    List<Integer> findByOrderId(int orderId);

}
