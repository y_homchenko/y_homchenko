package dao;

import model.CustomerOrder;

public interface CustomersOrdersDAO {

    void add(CustomerOrder customerOrder);

    CustomerOrder findByUserId(int userId);

    void update(CustomerOrder customerOrder);
}
