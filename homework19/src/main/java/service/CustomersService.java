package service;

import dao.CustomersDAO;
import model.Customers;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import servlets.CheckServlet;

/**
 * Customers table service
 */

public class CustomersService implements CustomersDAO {

    private final JdbcTemplate jdbcTemplate;
    private final Logger LOGGER = Logger.getLogger(CheckServlet.class);
    private final String LOG_CHECK = "CustomersService: ";

    public CustomersService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Insert instance Customers to table CUSTOMERS.
     *
     * @param customer - instance Customers.
     */
    @Override
    public void add(final Customers customer) {
        LOGGER.info(LOG_CHECK + "add");
        final String sql = "INSERT INTO CUSTOMERS (LOGIN, PASSWORD) VALUES(?, ?)";
        jdbcTemplate.update(sql, customer.getLogin(), customer.getPassword());
    }

    /**
     * Find the element of table CUSTOMERS by name.
     *
     * @param login - String login of the element.
     * @return - instance of Customers.
     */
    @Override
    public Customers findByName(final String login) {
        LOGGER.info(LOG_CHECK + "findByName");
        final String sql = "SELECT * FROM CUSTOMERS WHERE LOGIN=?";
        return jdbcTemplate.query(
                sql,
                new Object[]{login},
                new BeanPropertyRowMapper<>(Customers.class)).stream().findAny().orElse(null);
    }
}
