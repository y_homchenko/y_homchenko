package service;

import dao.GoodsDAO;
import model.Goods;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import servlets.CheckServlet;

import java.util.List;

/**
 * Goods table service.
 */
public class GoodsService implements GoodsDAO {

    private final Logger LOGGER = Logger.getLogger(CheckServlet.class);
    private final String LOG_CHECK = "GoodsService: ";
    private final JdbcTemplate jdbcTemplate;

    public GoodsService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Insert instance Goods to table GOODS.
     *
     * @param good - instance Goods.
     */
    @Override
    public void add(final Goods good) {
        LOGGER.info(LOG_CHECK + "add");
        final String sql = "INSERT INTO GOODS (TITLE, PRICE) VALUES(?, ?)";
        jdbcTemplate.update(sql, good.getTitle(), good.getPrice());
    }

    /**
     * Get all elements of table GOODS.
     *
     * @return - List elements of table GOODS.
     */
    @Override
    public List<Goods> getAll() {
        LOGGER.info(LOG_CHECK + "getAll");
        final String sql = "SELECT * FROM GOODS";
        return jdbcTemplate.query(
                sql,
                new BeanPropertyRowMapper<>(Goods.class)
        );
    }

    /**
     * Find the element of table GOODS by title.
     *
     * @param title - String title of the element.
     * @return - instance of Goods.
     */
    @Override
    public Goods findByTitle(final String title) {
        LOGGER.info(LOG_CHECK + "findByTitle");
        final String sql = "SELECT * FROM GOODS WHERE TITLE=?";
        return jdbcTemplate.query(sql,
                new Object[]{title},
                new BeanPropertyRowMapper<>(Goods.class)).stream().findAny().orElse(null);
    }

    /**
     * Find the element of table GOODS by ID.
     *
     * @param id - int id the element of table GOODS.
     * @return - instance of Goods.
     */
    @Override
    public Goods findById(final int id) {
        LOGGER.info(LOG_CHECK + "findById");
        final String sql = "SELECT * FROM GOODS WHERE ID=?";
        return jdbcTemplate.query(sql,
                new Object[]{id},
                new BeanPropertyRowMapper<>(Goods.class)).stream().findAny().orElse(null);
    }
}
