package service;

import dao.OrdersGoodsDAO;
import model.OrdersGoods;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import servlets.CheckServlet;

import java.util.List;

/**
 * OrdersGoods table service
 */
public class OrdersGoodsService implements OrdersGoodsDAO {

    private final Logger LOGGER = Logger.getLogger(CheckServlet.class);
    private final String LOG_CHECK = "OrdersGoodsService: ";
    private final JdbcTemplate jdbcTemplate;


    public OrdersGoodsService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Insert instance OrdersGoods to table ORDERS_GOODS.
     *
     * @param orderGood - instance OrdersGoods
     */
    @Override
    public void add(OrdersGoods orderGood) {
        LOGGER.info(LOG_CHECK + "add");
        String sql = "INSERT INTO ORDERS_GOODS (ORDER_ID, GOOD_ID) VALUES(?, ?)";
        jdbcTemplate.update(sql, orderGood.getOrderId(), orderGood.getGoodId());
    }

    /**
     * Find elements by order_id.
     *
     * @param orderId - int order_id
     * @return - List elements of table ORDERS_GOODS
     */
    @Override
    public List<Integer> findByOrderId(int orderId) {
        LOGGER.info(LOG_CHECK + "findByOrderId");
        String sql = "SELECT GOOD_ID FROM ORDERS_GOODS WHERE ORDER_ID=?";
        return jdbcTemplate.queryForList(sql, new Object[]{orderId}, Integer.class);
    }
}
