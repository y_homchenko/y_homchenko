package service;

import dao.CustomersOrdersDAO;
import model.CustomerOrder;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import servlets.CheckServlet;

/**
 * CustomersOrders table service.
 */
public class CustomersOrdersService implements CustomersOrdersDAO {

    private final Logger LOGGER = Logger.getLogger(CheckServlet.class);
    private final String LOG_CHECK = "CustomersOrdersService: ";
    private final JdbcTemplate jdbcTemplate;

    public CustomersOrdersService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    /**
     * Insert instance customerOrder to table CUSTOMERS_ORDERS.
     *
     * @param customerOrder - instance customerOrder.
     */
    @Override
    public void add(final CustomerOrder customerOrder) {
        LOGGER.info(LOG_CHECK + "add");
        final String sql = "INSERT INTO CUSTOMERS_ORDERS (USER_ID, TOTAL_PRICE) VALUES(?, ?)";
        jdbcTemplate.update(sql, customerOrder.getUserId(), customerOrder.getTotalPrice());
    }

    /**
     * Find the element of table CUSTOMERS_ORDERS by user_id.
     *
     * @param userId - int value the element of table CUSTOMERS_ORDERS.
     * @return - instance customerOrder.
     */
    @Override
    public CustomerOrder findByUserId(final int userId) {
        LOGGER.info(LOG_CHECK + "findByUserId");
        final String sql = "SELECT * FROM CUSTOMERS_ORDERS WHERE USER_ID=?";
        return jdbcTemplate.query(sql,
                new Object[]{userId},
                new BeanPropertyRowMapper<>(CustomerOrder.class)).stream().findAny().orElse(null);
    }

    /**
     * Update the element of table CUSTOMERS_ORDERS.
     *
     * @param customerOrder - instance customerOrder.
     */
    @Override
    public void update(final CustomerOrder customerOrder) {
        LOGGER.info(LOG_CHECK + "update");
        final String sql = "UPDATE CUSTOMERS_ORDERS SET TOTAL_PRICE=? WHERE ID=?";
        jdbcTemplate.update(sql, customerOrder.getUserId(), customerOrder.getTotalPrice());
    }
}
