package filters;

import model.CheckAgree;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class filter checks checkBoxAgree
 */
public class AgreeFilter implements Filter {
    private final Logger LOGGER = Logger.getLogger(AgreeFilter.class);
    private final String LOG_FILTER_AGREE = "FilterAgree: ";
    private final String ERROR_PATH = "/error";

    public void init(FilterConfig config) throws ServletException {
    }

    /**
     * Handles {@link Filter} doFilter Method
     *
     * @param request  - request the {@link ServletRequest}
     * @param response - response the {@link ServletResponse}
     * @param chain    - chain the {@link FilterChain}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        final HttpSession session = ((HttpServletRequest) request).getSession();
        final CheckAgree checkAgree = (CheckAgree) session.getAttribute("checkAgree");
        if (!checkAgree.isCheckAgree()) {
            LOGGER.info(LOG_FILTER_AGREE + "checkBoxAgree is null");
            try {
                request.getRequestDispatcher(ERROR_PATH).forward(request, response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    public void destroy() {
    }
}
