<%@ page import="model.Customers" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext" %>
<%@ page import="service.CustomersOrdersService" %>
<%@ page import="util.PrintList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Customers customer = (Customers) session.getAttribute("customer");
    final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    CustomersOrdersService customersOrdersService = context.getBean("customersOrdersService", CustomersOrdersService.class);
    int id = customersOrdersService.findByUserId(customer.getId()).getUserId();
%>
<html>
<head>
    <title>Check</title>
</head>
<h1>Dear <%=customer.getLogin()%>, your order:</h1>
<body>
<h4>Make you customerOrder:</h4>
<%=new PrintList().listOrdersGoodsWithGoodsToString(id)%>
</body>
</html>
