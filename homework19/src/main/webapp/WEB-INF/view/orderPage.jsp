<%@ page import="model.Customers" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext" %>
<%@ page import="service.CustomersOrdersService" %>
<%@ page import="util.PrintList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf8">
    <title>Order</title>
</head>
<%
    Customers customer = (Customers) session.getAttribute("customer");
    final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    CustomersOrdersService customersOrdersService = context.getBean("customersOrdersService", CustomersOrdersService.class);
    int id = customersOrdersService.findByUserId(customer.getId()).getUserId();
%>
<h1>Hello <%=customer.getLogin()%>!</h1>
<body>
<h4>You have already chosen:</h4>
<p><%=new PrintList().listOrdersGoodsWithGoodsToString(id)%>
</p>
<form action="order" method="POST">
    <h4>Make you order:</h4>
    <p><b>Goods:</b><br> <select id="GOODS" name="goods">
        <%=new PrintList().listGoodsToString()%>
    </select></p>
    <input type="submit" name="addItem" value="Add item">
    <input type="submit" name="submit" value="Submit">
</form>
</body>
</html>
