package filters;

import model.CheckAgree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AgreeFilterTest {

    @Mock
    private CheckAgree checkAgree;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private FilterChain filterChain;
    @Mock
    private RequestDispatcher dispatcher;

    @Test
    public void testDoFilterCheckBoxAgreeTrue() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("checkAgree")).thenReturn(checkAgree);
        when(checkAgree.isCheckAgree()).thenReturn(false);
        when(request.getRequestDispatcher("/error")).thenReturn(dispatcher);

        //when:
        new AgreeFilter().doFilter(request, response, filterChain);

        //then:
        verify(request.getRequestDispatcher("/error")).forward(request, response);
    }

    @Test
    public void testDoFilterCheckBoxAgreeFalse() throws ServletException, IOException {
        //given:
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("checkAgree")).thenReturn(checkAgree);
        when(checkAgree.isCheckAgree()).thenReturn(true);

        //when:
        new AgreeFilter().doFilter(request, response, filterChain);

        //then:
        verify(filterChain).doFilter(request, response);
    }
}
