package servlets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CheckServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    @Test
    public void testDoGet() throws IOException, ServletException {

        //given:
        when(request.getRequestDispatcher("/WEB-INF/view/checkPage.jsp")).thenReturn(dispatcher);

        //when:
        new CheckServlet().doGet(request, response);

        //then:
        verify(request.getRequestDispatcher("/WEB-INF/view/checkPage.jsp")).forward(request, response);
    }
}
