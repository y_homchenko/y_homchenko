package servlets;

import model.CustomerOrder;
import model.Customers;
import model.Goods;
import model.OrdersGoods;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import service.CustomersOrdersService;
import service.GoodsService;
import service.OrdersGoodsService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private GoodsService goodsService;
    @Mock
    private Customers customers;
    @Mock
    private CustomersOrdersService customersOrdersService;
    @Mock
    private CustomerOrder customerOrder;
    @Mock
    private Goods goods;
    @Mock
    private ApplicationContext context;
    @Mock
    private OrdersGoodsService ordersGoodsService;
    @Mock
    private OrdersGoods ordersGoods;


    @Test
    public void testDoGet() throws ServletException, IOException {

        //given:
        when(request.getRequestDispatcher("/WEB-INF/view/orderPage.jsp")).thenReturn(dispatcher);

        //when:
        new OrderServlet().doGet(request, response);

        //then:
        verify(request.getRequestDispatcher("/WEB-INF/view/orderPage.jsp")).forward(request, response);
    }

    @Test
    public void testDoPostRedirectCheck() throws ServletException, IOException {

        //given:
        String item = "test 5$";
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("goods")).thenReturn(item);
        when(request.getParameter("submit")).thenReturn("submit");

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/check");
    }

    @Test
    public void testDoPost() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("goods")).thenReturn("table 5$");
        when(request.getParameter("addItem")).thenReturn("addItem");
        when(context.getBean("goodsService", GoodsService.class)).thenReturn(goodsService);
        when(context.getBean("ordersGoodsService", OrdersGoodsService.class)).thenReturn(ordersGoodsService);
        when(context.getBean("ordersGoods", OrdersGoods.class)).thenReturn(ordersGoods);
        when(session.getAttribute("customer")).thenReturn(customers);
        when(customers.getId()).thenReturn(1);
        when(context.getBean("customersOrdersService", CustomersOrdersService.class)).thenReturn(customersOrdersService);
        when(customersOrdersService.findByUserId(1)).thenReturn(customerOrder);
        when(customerOrder.getId()).thenReturn(1);
        when(customerOrder.getTotalPrice()).thenReturn(5d);
        when(goodsService.findByTitle(any())).thenReturn(goods);
        when(goods.getId()).thenReturn(1);

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }

    @Test
    public void testDoPostItemsIsNull() throws ServletException, IOException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("addItem")).thenReturn("addItem");
        when(request.getParameterValues("goods")).thenReturn(null);

        //when:
        new OrderServlet().doPost(request, response);

        //then:
        verify(response).sendRedirect("/order");
    }
}
