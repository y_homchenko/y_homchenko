package servlets;

import model.CheckAgree;
import model.Customers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import service.CustomersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MyShopServletTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private Customers customer;
    @Mock
    private CustomersService customersService;
    @Mock
    private ApplicationContext context;
    @Mock
    private CustomersService customerService;
    @Mock
    private CheckAgree checkAgree;

    @Test
    public void testDoGet() throws IOException, ServletException {

        //given:
        when(request.getRequestDispatcher("/WEB-INF/view/startPage.jsp")).thenReturn(dispatcher);

        //when:
        new MyShopServlet().doGet(request, response);

        //then:
        verify(request.getRequestDispatcher("/WEB-INF/view/startPage.jsp")).forward(request, response);
    }

    @Test
    public void testDoPost() throws IOException, ServletException {
        //given:
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("nameCustomer")).thenReturn("User1");
        when(context.getBean("customers", Customers.class)).thenReturn(customer);
        when(context.getBean("customersService", CustomersService.class)).thenReturn(customerService);
        when(context.getBean("checkAgree", CheckAgree.class)).thenReturn(checkAgree);
        when(customersService.findByName("User1")).thenReturn(customer);
        when(request.getParameter("checkBoxAgree")).thenReturn("true");
        when(request.getRequestDispatcher("/order")).thenReturn(dispatcher);

        //when:
        new MyShopServlet().doPost(request, response);

        //then:
        verify(request.getRequestDispatcher("/order")).forward(request, response);
    }

    @Test
    public void testDoPostFindByNameIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("nameCustomer")).thenReturn(null);
        when(customersService.findByName("User1")).thenReturn(null);
        when(request.getRequestDispatcher("/WEB-INF/view/startPage.jsp")).thenReturn(dispatcher);

        //when:
        new MyShopServlet().doPost(request, response);

        //then:
        verify(request.getRequestDispatcher("/WEB-INF/view/startPage.jsp")).forward(request, response);
    }

    @Test
    public void testDoPostNameCustomerAndCheckBoxAgreeIsNull() throws IOException, ServletException {

        //given:
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("nameCustomer")).thenReturn("User1");
        when(customersService.findByName("User1")).thenReturn(customer);
        when(request.getParameter("checkBoxAgree")).thenReturn(null);
        when(request.getRequestDispatcher("/order")).thenReturn(dispatcher);

        //when:
        new MyShopServlet().doPost(request, response);

        //then:
        verify(request.getRequestDispatcher("/order")).forward(request, response);
    }
}
