package model;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * Customer cart
 * nameCustomer - buyer's name
 * order - shopping cart
 */
public class Cart {
    private final String nameCustomer;
    private ConcurrentMap<String, Double> order;

    public Cart(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public ConcurrentMap<String, Double> getOrder() {
        return order;
    }

    public void setOrder(ConcurrentMap<String, Double> order) {
        this.order = order;
    }

    @Override
    public String toString() {
        int i = 1;
        Double total = 0.0;
        StringBuilder result = new StringBuilder();
        if (getOrder() != null) {
            for (Map.Entry entry : getOrder().entrySet()) {
                result.append("<p>").append(i).append(" ").append(entry.getKey()).append(" ").append(entry.getValue()).append("$</p>");
                total += ((Double) entry.getValue());
                i++;
            }
        }
        result.append("<h4>Total: $ ").append(total).append("</h4>");
        return result.toString();
    }
}
