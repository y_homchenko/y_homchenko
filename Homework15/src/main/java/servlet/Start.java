package servlet;

import model.Cart;
import org.apache.log4j.Logger;
import service.ValidatorName;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class servlet start page
 */
public class Start extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(Start.class);
    private final String REDIRECT_PATH = "/order";
    private final String LOG_START = "StartServlet: ";
    private final String START_PATH = "/start";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_START + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_START + "doGet");
        final String body =
                "<html>"
                        + "<head>"
                        + "<meta charset='utf8'>"
                        + "</head>"
                        + "<h1>Welcome to Online Shop</h1>"
                        + "<body>"
                        + "<form action='start' method='POST'>"
                        + "<p><b>Enter your name:</b><br>"
                        + "<input type='text' name='nameCustomer'></p>"
                        + "<input type='submit' value= 'Enter'>"
                        + "</form>"
                        + "</body>"
                        + "</html>";
        try {
            response.getWriter().println(body);
        } catch (IOException e) {
            LOGGER.error(LOG_START + e);
        }
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_START + "doPost");
        final HttpSession session = request.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        String nameCustomer = request.getParameter("nameCustomer");
        final ValidatorName validatorName = new ValidatorName();
        nameCustomer = validatorName.checkingName(nameCustomer);
        try {
            if (!nameCustomer.equals("")) {
                if (cart == null) {
                    cart = new Cart(nameCustomer);
                    session.setAttribute("cart", cart);
                }
                response.sendRedirect(REDIRECT_PATH);
            } else {
                LOGGER.info(LOG_START + "name is null");
                response.sendRedirect(START_PATH);

            }
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error(LOG_START + e);
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_START + "destroy");
    }
}
