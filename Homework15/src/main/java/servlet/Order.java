package servlet;

import model.Cart;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Order extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(Order.class);
    private final String REDIRECT_PATH = "/check";
    private final String REDIRECT_PATH_START = "/start";
    private final String ORDER_PATH = "/order";
    private final String LOG_ORDER = "OrderServlet: ";

    @Override
    public void init(final ServletConfig config) {
        LOGGER.info(LOG_ORDER + "init");
    }

    /**
     * Handles {@link HttpServlet} GET Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ORDER + "doGet");
        final HttpSession session = request.getSession();
        final Cart cart = (Cart) session.getAttribute("cart");
        if (cart == null) {
            LOGGER.error(LOG_ORDER + "cart is null");
            try {
                response.sendRedirect(REDIRECT_PATH_START);
            } catch (IOException e) {
                LOGGER.error(LOG_ORDER + e);
            }
        } else {
            final String body =
                    "<html>"
                            + "<head>"
                            + "<meta charset='utf8'>"
                            + "</head>"
                            + "<h1>Hello "
                            + cart.getNameCustomer()
                            + "!</h1>"
                            + "<body>"
                            + "<form action='order' method='POST'>"
                            + "<h4>Make you order:</h4>"
                            + "<p><b>Goods:</b><br> <select id='GOODS' name='goods' multiple size='3'>"
                            + "<option>Book 5$</option>"
                            + "<option>Phone 150$</option>"
                            + "<option>TV 500$</option>"
                            + "<option>Table 30$</option>"
                            + "<option>Chair 50$</option>"
                            + "</select></p>"
                            + "<input type='submit' value= 'Submit'>"
                            + "</form>"
                            + "</body>"
                            + "</html>";
            try {
                response.getWriter().println(body);
            } catch (IOException e) {
                LOGGER.error(LOG_ORDER + e);
            }
        }
    }

    /**
     * Handles {@link HttpServlet} POST Method
     *
     * @param request  - request the {@link HttpServletRequest}
     * @param response - response the {@link HttpServletResponse}
     * @throws ServletException - the Servlet exception
     * @throws IOException      - the IO exception
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(LOG_ORDER + "doPost");
        final HttpSession session = request.getSession();
        final Cart cart = (Cart) session.getAttribute("cart");
        final String[] items = request.getParameterValues("goods");
        final ConcurrentMap<String, Double> goods = new ConcurrentHashMap<>();
        if (items != null) {
            for (String i : items) {
                String[] str = i.split(" ");
                try {
                    goods.put(str[0], Double.parseDouble(str[1].replace("$", "")));
                } catch (NumberFormatException e) {
                    LOGGER.error("NumberFormatException " + e);
                    e.printStackTrace();
                }
            }
            cart.setOrder(goods);
            session.setAttribute("cart", cart);
            try {
                response.sendRedirect(REDIRECT_PATH);
            } catch (IOException e) {
                LOGGER.error(LOG_ORDER + e);
            }
        } else {
            LOGGER.info(LOG_ORDER + "items is null");
            try {
                response.sendRedirect(ORDER_PATH);
            } catch (IOException e) {
                LOGGER.error(LOG_ORDER + e);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroy() {
        LOGGER.info(LOG_ORDER + "destroy");
    }
}
