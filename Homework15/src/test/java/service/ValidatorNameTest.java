package service;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValidatorNameTest {

    final ValidatorName validatorName = new ValidatorName();

    @Test
    public void testCheckingName() {
        String expected = "MyName";
        String actual = validatorName.checkingName("MyName");
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckingNameLeftAngleBracket() {
        String expected = "&lt;MyName";
        String actual = validatorName.checkingName("<MyName");
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckingNameScript() {
        String expected = "&lt;script&gt;alert('Hello')&lt;/script&gt;";
        String actual = validatorName.checkingName("<script>alert('Hello')</script>");
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckingNameRightAngleBracket() {
        String expected = "MyName&gt;";
        String actual = validatorName.checkingName("MyName>");
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckingNameLeftAndRightAngleBracket() {
        String expected = "&lt;MyName&gt;";
        String actual = validatorName.checkingName("<MyName>");
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckingNameNull() {
        String expected = "";
        String actual = validatorName.checkingName(null);
        assertEquals(expected, actual);
    }
}
