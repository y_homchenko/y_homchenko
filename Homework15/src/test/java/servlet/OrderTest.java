package servlet;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession session;
    @Mock
    PrintWriter writer;
    @Mock
    Cart cart;

    @Test
    public void testDoGet() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(response.getWriter()).thenReturn(writer);
        new Order().doGet(request, response);
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoGetCartIsNull() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(null);
        new Order().doGet(request, response);
        verify(response).sendRedirect("/start");
    }

    @Test
    public void testDoPost() throws ServletException, IOException {
        String[] items = new String[]{"test 5$"};
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameterValues("goods")).thenReturn(items);
        new Order().doPost(request, response);
        verify(response).sendRedirect("/check");
    }

    @Test
    public void testDoPostItemsIsNull() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameterValues("goods")).thenReturn(null);
        new Order().doPost(request, response);
        verify(response).sendRedirect("/order");
    }
}
