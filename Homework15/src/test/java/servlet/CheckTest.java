package servlet;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CheckTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession session;
    @Mock
    PrintWriter writer;
    @Mock
    Cart cart;

    @Test
    public void testDoGet() throws IOException, ServletException {
        final ConcurrentHashMap<String, Double> order = new ConcurrentHashMap<>();
        order.put("test", 5d);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(cart.getOrder()).thenReturn(order);
        when(response.getWriter()).thenReturn(writer);
        new Check().doGet(request, response);
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoGetCartIsNull() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(null);
        new Check().doGet(request, response);
        verify(response).sendRedirect("/start");
    }

    @Test
    public void testDoGetOrderIsNull() throws IOException, ServletException {
        //final ConcurrentHashMap<String, Double> order = null;
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(cart.getNameCustomer()).thenReturn("Test");
        when(cart.getOrder()).thenReturn(null);
        new Check().doGet(request, response);
        verify(response).sendRedirect("/order");
    }
}
