package servlet;

import model.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class StartTest {

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession session;
    @Mock
    PrintWriter writer;
    @Mock
    Cart cart;

    @Test
    public void testDoGet() throws IOException, ServletException {
        when(response.getWriter()).thenReturn(writer);
        new Start().doGet(request, response);
        verify(response).getWriter();
        verify(writer).println(anyString());
    }

    @Test
    public void testDoPost() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn("Test");
        new Start().doPost(request, response);
        verify(response).sendRedirect("/order");
    }

    @Test
    public void testDoPostCartIsNull() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("cart")).thenReturn(cart);
        when(request.getParameter("nameCustomer")).thenReturn(null);
        new Start().doPost(request, response);
        verify(response).sendRedirect("/start");
    }
}
