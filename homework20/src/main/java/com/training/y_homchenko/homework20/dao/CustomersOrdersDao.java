package com.training.y_homchenko.homework20.dao;

import com.training.y_homchenko.homework20.domain.CustomerOrder;

public interface CustomersOrdersDao {

    CustomerOrder findByUserId(Long userId);

    void update(CustomerOrder customerOrder);
}
