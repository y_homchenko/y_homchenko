package com.training.y_homchenko.homework20.domain;

import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Customers {

    private Long id;
    private String username;
    private String password;
    private String role;

    public Customers() {
    }

    public Customers(Long id, String login, String password, String role) {
        this.id = id;
        this.username = login;
        this.password = password;
        this.role = role;
    }

    public Customers(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customers customers = (Customers) o;
        return Objects.equals(id, customers.id) &&
                Objects.equals(username, customers.username) &&
                Objects.equals(password, customers.password) &&
                Objects.equals(role, customers.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, role);
    }

    @Override
    public String toString() {
        return "Customers{" +
                "id=" + id +
                ", login='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
