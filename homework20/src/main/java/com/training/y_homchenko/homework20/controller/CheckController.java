package com.training.y_homchenko.homework20.controller;

import com.training.y_homchenko.homework20.dao.CustomersDao;
import com.training.y_homchenko.homework20.domain.Customers;
import com.training.y_homchenko.homework20.service.PrintListService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * Controller customer check
 */
@Controller
public class CheckController {

    private final PrintListService printListService;
    private final CustomersDao customersDao;

    public CheckController(PrintListService printListService, CustomersDao customersDao) {
        this.printListService = printListService;
        this.customersDao = customersDao;
    }

    @RequestMapping(value = "checkPage", method = RequestMethod.GET)
    public String doGet(HttpSession session, Authentication authentication) {
        Customers customer = customersDao.findByName(authentication.getName());
        String goodsCustomerAlready = printListService.listOrdersGoodsWithGoodsToString(customer);
        session.setAttribute("goodsCustomerAlready", goodsCustomerAlready);
        session.setAttribute("customerName", customer.getUsername());
        return "checkPage";
    }
}
