package com.training.y_homchenko.homework20.dao.impl;

import com.training.y_homchenko.homework20.dao.CustomersDao;
import com.training.y_homchenko.homework20.domain.Customers;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Customers table implementation CustomersDao.
 */

@Repository
public class CustomersImpl implements CustomersDao {

    private final String SQL_FIND_BY_NAME = "SELECT * FROM CUSTOMERS WHERE USERNAME=?";

    private final JdbcTemplate jdbcTemplate;

    public CustomersImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Find the element of table CUSTOMERS by name.
     *
     * @param login - String login of the element.
     * @return - instance of Customers.
     */
    @Override
    public Customers findByName(final String login) {
        return jdbcTemplate.query(
                SQL_FIND_BY_NAME,
                new Object[]{login},
                new BeanPropertyRowMapper<>(Customers.class)).stream().findAny().orElse(null);
    }
}
