package com.training.y_homchenko.homework20.dao;

import com.training.y_homchenko.homework20.domain.Customers;

public interface CustomersDao {

    Customers findByName(String name);
}
