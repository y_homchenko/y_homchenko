package com.training.y_homchenko.homework20.dao.impl;

import com.training.y_homchenko.homework20.dao.GoodsDao;
import com.training.y_homchenko.homework20.domain.Goods;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Goods table implementation GoodsDao.
 */
@Repository
public class GoodsImpl implements GoodsDao {

    private final String SQL_GET_ALL = "SELECT * FROM GOODS";
    private final String SQL_FIND_BY_TITLE = "SELECT * FROM GOODS WHERE TITLE=?";
    private final String SQL_FIND_BY_ID = "SELECT * FROM GOODS WHERE ID=?";

    private final JdbcTemplate jdbcTemplate;

    public GoodsImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get all elements of table GOODS.
     *
     * @return - List elements of table GOODS.
     */
    @Override
    public List<Goods> getAll() {
        return jdbcTemplate.query(
                SQL_GET_ALL,
                new BeanPropertyRowMapper<>(Goods.class)
        );
    }

    /**
     * Find the element of table GOODS by title.
     *
     * @param title - String title of the element.
     * @return - instance of Goods.
     */
    @Override
    public Goods findByTitle(final String title) {
        return jdbcTemplate.query(SQL_FIND_BY_TITLE,
                new Object[]{title},
                new BeanPropertyRowMapper<>(Goods.class)).stream().findAny().orElse(null);
    }

    /**
     * Find the element of table GOODS by ID.
     *
     * @param id - Long id the element of table GOODS.
     * @return - instance of Goods.
     */
    @Override
    public Goods findById(final Long id) {
        return jdbcTemplate.query(SQL_FIND_BY_ID,
                new Object[]{id},
                new BeanPropertyRowMapper<>(Goods.class)).stream().findAny().orElse(null);
    }
}
