package com.training.y_homchenko.homework20.dao.impl;

import com.training.y_homchenko.homework20.dao.CustomersOrdersDao;
import com.training.y_homchenko.homework20.domain.CustomerOrder;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * CustomersOrders table implementation CustomersOrdersDao.
 */
@Repository
public class CustomersOrdersImpl implements CustomersOrdersDao {

    private final String SQL_FIND_BY_USER_ID = "SELECT * FROM CUSTOMERS_ORDERS WHERE USER_ID=?";
    private final String SQL_UPDATE = "UPDATE CUSTOMERS_ORDERS SET TOTAL_PRICE=? WHERE ID=?";

    private final JdbcTemplate jdbcTemplate;

    public CustomersOrdersImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Find the element of table CUSTOMERS_ORDERS by user_id.
     *
     * @param userId - Long value the element of table CUSTOMERS_ORDERS.
     * @return - instance customerOrder.
     */
    @Override
    public CustomerOrder findByUserId(final Long userId) {
        return jdbcTemplate.query(SQL_FIND_BY_USER_ID,
                new Object[]{userId},
                new BeanPropertyRowMapper<>(CustomerOrder.class)).stream().findAny().orElse(null);
    }

    /**
     * Update the element of table CUSTOMERS_ORDERS.
     *
     * @param customerOrder - instance customerOrder.
     */
    @Override
    public void update(final CustomerOrder customerOrder) {
        jdbcTemplate.update(SQL_UPDATE, customerOrder.getUserId(), customerOrder.getTotalPrice());
    }
}
