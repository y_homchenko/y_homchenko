<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String goodsCustomerAlready = (String) session.getAttribute("goodsCustomerAlready");
    String listGoods = (String) session.getAttribute("listGoods");
    String customerName = (String) session.getAttribute("customerName");
%>

<html>
<head>
    <meta charset="utf8">
    <title>Order</title>
</head>
<h1>Hello <%=customerName%>!</h1>
<body>
<form method="POST" action="checkGoods">
    <h4>You have already chosen:</h4>
    <p><%=goodsCustomerAlready%>
    </p>
    <h4>Make you order:</h4>
    <p><b>Goods:</b><br> <select id="GOODS" name="goods">
        <%=listGoods%>
    </select></p>
    <input type="submit" name="addItem" value="Add item">
    <input type="submit" name="submit" value="Submit">
</form>
</body>
</html>
