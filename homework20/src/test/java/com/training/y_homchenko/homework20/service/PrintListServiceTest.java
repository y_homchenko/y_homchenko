package com.training.y_homchenko.homework20.service;

import com.training.y_homchenko.homework20.config.WebConfig;
import com.training.y_homchenko.homework20.domain.Customers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = WebConfig.class)
public class PrintListServiceTest {
    @Autowired
    private PrintListService printListService;

    @Test
    public void TestListGoodsToString() {
        //given:
        String expected = "<option>table 50.00$</option><option>phone 150.00$</option><option>laptop 500.00$</option><option>book 5.00$</option><option>chair 30.00$</option>";
        //when:
        String actual = printListService.listGoodsToString();
        //then:
        assertEquals(expected, actual);
    }

    @Test
    public void testListOrdersGoodsWithGoodsToString() {
        //given:
        Customers customer = new Customers();
        customer.setId(1L);
        customer.setUsername("User1");
        customer.setPassword("$2a$04$i/eDtmGlaU222OKvA8/YHuhW8rpf/XmCEGWmhU/PnSjTEJsEXTany");
        customer.setRole("ROLE_ADMIN");
        String expected = "<p>1) table 50.00$</p><h4>Total: $ 50.00</h4>";
        //when:
        String actual = printListService.listOrdersGoodsWithGoodsToString(customer);
        //then:
        assertEquals(expected, actual);
    }
}